<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pro1_pro2_status')->nullable();
            $table->string('project_nameth');
            $table->string('project_nameen');
            $table->string('project_detailth');
            $table->string('project_detailen');
            $table->string('file_project')->nullable();
            $table->string('file_full')->nullable();
            $table->string('status_finished_notfinished')->nullable();
            $table->string('status_topic_adviser')->nullable();
            $table->string('status_topic_admin')->nullable();
            $table->string('status_midterm')->nullable();
            $table->string('status_final')->nullable();
            $table->integer('projecttype_id')->unsigned();
            $table->foreign('projecttype_id')->references('id')->on('project_types')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
