<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Project;
use App\studentadexam;

class AdminController extends Controller
{


    public function allproject()
    {
        $data['project'] = DB::table('projects')
            ->leftJoin('studentadexams', 'projects.id', 'studentadexams.project_id')
            ->select(
                'studentadexams.id',
                'projects.pro1_pro2_status',
                'projects.project_nameth',
                'projects.project_nameen',
                'projects.project_detailth',
                'projects.project_detailen',
                'projects.file_project',
                'projects.file_full',
                'projects.status_finished_notfinished',
                'projects.status_topic_adviser',
                'projects.status_topic_admin',
                'projects.status_midterm',
                'projects.status_final',
                'projects.projecttype_id',
                'studentadexams.status_exam_adviser',
                'studentadexams.filefirst',
                'studentadexams.fileend',
                'studentadexams.status_exam_admin',
                'studentadexams.status_exam',
                'studentadexams.project_id',
                'studentadexams.exam_id'
            )->get();
        return view('page.admin.allproject', $data);
    }
    public function allprojectAdmin()
    {
        $data['project'] = DB::table('projects')
            ->leftJoin('studentadexams', 'projects.id', 'studentadexams.project_id')
            ->select(
                'studentadexams.id',
                'projects.pro1_pro2_status',
                'projects.project_nameth',
                'projects.project_nameen',
                'projects.project_detailth',
                'projects.project_detailen',
                'projects.file_project',
                'projects.file_full',
                'projects.status_finished_notfinished',
                'projects.status_topic_adviser',
                'projects.status_topic_admin',
                'projects.status_midterm',
                'projects.status_final',
                'projects.projecttype_id',
                'studentadexams.status_exam_adviser',
                'studentadexams.filefirst',
                'studentadexams.fileend',
                'studentadexams.status_exam_admin',
                'studentadexams.status_exam',
                'studentadexams.project_id',
                'studentadexams.exam_id'
            )->get();
        return view('page.profesor.allprojectAdmin', $data);
    }

    public function detel(Request $request, $project_name)
    {

        $ids = DB::table('projects')->select('id')
            ->where('project_name', '=', $project_name)->first();

        $data['projectss'] = DB::table('projects')
            ->where('project_name', '=', $project_name)
            ->get();

        $data['student'] = DB::table('membersprojects')
            ->join('students', 'membersprojects.students_id', 'students.id')

            ->where('membersprojects.projects_id', '=', $ids->id)
            // ->groupBy('membersprojects.students_id')
            ->get();
        //  dd( $data['student']);
        return view('page.profesor.detel', $data);
    }

    public function index()
    {
        // $id =  Auth::user()->id;
        // $idc = DB::table('officer_groupprojecttypes')->select('id')
        //     ->where('officer_id', '=', $ids->id)->first();
        // dd($ids->id);


        $data['project'] = DB::table('projects')
            ->where('status_midterm', '=', 'ขอสอบmidterm')
            ->get();
        // dd($data['advisers']);
        return view('page.admin.requestexam', $data);
    }

    public function upstatus1(Request $request, $project_name)
    {
        //   dd($project_name);
        $ids = DB::table('projects')->select('id')
            ->where('project_name', '=', $project_name)->first();
        //   dd($ids);
        $validatedData = $request->validate([]);
        $projects = Project::find($ids->id);
        $projects->status_midterm = $request->get('status_midterm');
        $projects->save();
        return back()->with('success', 'บันทึกสำเร็จ');
    }

    public function upstatus2(Request $request, $project_name)
    {
        //   dd($project_name);
        $ids = DB::table('projects')->select('id')
            ->where('project_name', '=', $project_name)->first();
        //   dd($ids);
        $validatedData = $request->validate([]);
        $projects = Project::find($ids->id);
        $projects->status_midterm = $request->get('status_midterm');
        $projects->save();
        return back()->with('success', 'บันทึกสำเร็จ');
    }
    public function upstatus_exam(Request $request, $project_id)
    {
        $ids = DB::table('projects')->select('id')
        ->where('id', '=', $project_id)->first();
        //dd($ids);
        $Project = Project::find($ids->id);
        $Project->pro1_pro2_status = $request->get('pro1_pro2_status');
        $Project->save();
        return back()->with('success', 'บันทึกสำเร็จ');
    }
}
