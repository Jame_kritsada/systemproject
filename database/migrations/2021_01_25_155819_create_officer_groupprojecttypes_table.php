<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficerGroupprojecttypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('officer_groupprojecttypes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('projecttype_id')->unsigned();
            $table->integer('officer_id')->unsigned();
            $table->timestamps();
            $table->foreign('projecttype_id')->references('id')->on('project_types')->onDelete('cascade');
            $table->foreign('officer_id')->references('id')->on('officers')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('officer_groupprojecttypes');
    }
}
