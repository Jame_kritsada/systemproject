@extends('Layout.masterstd')
@section('title')
@endsection('title')
@section('content')
    <div class="section-title">
        <h2>INFORMATION TECHNOLOGY</h2>
        <h4>&nbsp;&nbsp;ข้อมูลส่วนตัว</h4>
    </div>
    <div class="container">
        @foreach ($students as $row)
        <form action="{{ action('StdinformationController@update', $row->id) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                &nbsp;&nbsp;&nbsp;&nbsp;<h4>
                    <p class="font-weight-bold">ชื่อ : <input type="text" name="student_name" value="{{ $row->student_name }}">
                       สกุล : <input type="text" name="student_lastname" value="{{ $row->student_lastname }}"></p>
                    

              
            </div>
            <div class="row mt-0">
                &nbsp;&nbsp;&nbsp;&nbsp;<h4>
                    <p class="font-weight-bold">รหัสนิสิต :  <input type="text" name="student_id" value="{{ $row->student_id }}"></p>
                   
               
            </div>
            <div class="row mt-0">
                &nbsp;&nbsp;&nbsp;&nbsp;<h4>
                    <p class="font-weight-bold">เบอร์โทรศัพท์ :  <input type="text" name="tel_std" value="{{ $row->tel_std }}"></p>
                   

                
            </div>
           
                <div class=" text-center">
                    <button type="submit" class="btn btn-primary "
                        onclick="return confirm('ต้องการแก้ไขข้อมูลใช่ หรือ ไม่')">เพิ่มข้อมูล</button>
                </div>
                <input type="hidden" name="_method" value="PATCH">
            </form>
        @endforeach
    </div>

@endsection
