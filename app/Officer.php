<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{
    protected $fillable = [
        'officer_name',
        'officer_lastname',
        'tel_off',
        'user_id',
        ];
      protected $primaryKey = 'id';
} 
