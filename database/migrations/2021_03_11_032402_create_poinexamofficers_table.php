<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePoinexamofficersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poinexamofficers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('poin');
            $table->string('comment');
            $table->string('officer_name');
            $table->string('project_nameth');
            $table->string('exam_id');
            $table->integer('studentadexams_id')->unsigned();
            $table->integer('id_user')->unsigned();

            $table->timestamps();
            $table->foreign('studentadexams_id')->references('id')->on('studentadexams')->onDelete('cascade');
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poinexamofficers');
    }
}
