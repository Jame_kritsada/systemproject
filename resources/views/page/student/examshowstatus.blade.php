@extends('Layout.masterstd')
@section('title')
@endsection('title')
@section('content')
<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
</div>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link " href="{{ url('exam/show')}}" role="tab" aria-controls="pills-home">รายการสอบ</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="{{ url('exam/showstatus')}}" role="tab" aria-controls="pills-profile">สถานะสอบ</a>
    </li>

</ul>

<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">ชื่อโปรเจค</th>
            <th scope="col">ชื่อสอบ</th>
            <th scope="col">สถานะสอบ</th>
            <th scope="col">วันที่สอบ</th>


        </tr>
    </thead>
    <tbody>

        @foreach ($statusexams as $row)
        <tr>

            <td>{{ $row->project_nameth }}</td>
            @if($row->topic == 'midterm')
            <td>กลางภาค</td>
            @elseif($row->topic == 'final')
            <td>ปลายภาค</td>
            @endif

            @if ($row->status_exam_admin == 'ไม่ผ่านการอนุมัติ')
            <td>
                <div><a class="btn btn-danger">{{ $row->status_exam_admin }}</a>
                    <a href="{{ action('RequestexamController@edit', $row->id) }}" class="btn btn-danger">แก้ไขและส่งอีกครั้ง!</a>
                    <form method="post" class="delete_form" action="{{ action('RequestexamController@destroy', $row->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class="btn btn-danger">ลบ!</button>
                    </form>
                </div>
            </td>
            @elseif ($row->status_exam_adviser == 'ไม่ผ่านการอนุมัติ')
            <td> <a class="btn btn-danger">{{ $row->status_exam_adviser }}</a>
                <a href="{{ action('RequestexamController@edit', $row->id) }}" class="btn btn-danger">แก้ไขและส่งอีกครั้ง!</a>
                <form method="post" class="delete_form" action="{{ action('RequestexamController@destroy', $row->id) }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger">ลบ!</button>
                </form>
            </td>
            @elseif ($row->status_exam_admin == 'รอการอนุมัติ')
            <td><button type="text" class="btn btn-info">{{ $row->status_exam_admin }}</button></td>
            @elseif ($row->status_exam_admin == 'ผ่านการอนุมัติ')
            <td><button type="text" class="btn btn-success">{{ $row->status_exam_admin }}</button></td>
            @endif
            <td>{{ $row->dateexamstart }} ถึง {{ $row->dateexamend }}</td>




        </tr>



    </tbody>
    @endforeach
</table>

<style>
    .blink {
        animation: blinker 0.6s linear infinite;
        color: #1c87c9;
        font-size: 30px;
        font-weight: bold;
        font-family: sans-serif;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

    .blink-one {
        animation: blinker-one 1s linear infinite;
    }

    @keyframes blinker-one {
        0% {
            opacity: 0;
        }
    }

    .blink-two {
        animation: blinker-two 1.4s linear infinite;
    }

    @keyframes blinker-two {
        100% {
            opacity: 0;
        }
    }
</style>
@endsection