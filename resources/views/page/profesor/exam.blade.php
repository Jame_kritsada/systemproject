@extends('Layout.master')
@section('title')
หน้าแรก
@endsection('title')

@section('content')

<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
</div>

@foreach ($exams as $row)
<div class="card">
    <div class="card-body shadow">
        <dt>รายละเอียด</dt>
        <div class="form-row col-sm-12">
            <div class="form-group col-sm-2">
                @if($row->topic == 'midterm')
                <p class="mt-3" style=" color: rgb(60,60,60);">&nbsp;&nbsp;สอบกลางภาค</p>
                @elseif($row->topic == 'final')
                <p class="mt-3" style=" color:  rgb(60,60,60);">&nbsp;&nbsp;สอบปลายภาค</p>
                @endif
            </div>
            <div class="form-group row col-sm-3">
                <p class="mt-3" style=" color: rgb(10,10,10);">สอบวันที่</p>
                <p class="mt-3" style=" color: rgb(60,60,60);">&nbsp;{{$row->dateexamstart}}&nbsp;ถึง&nbsp;{{$row->dateexamend}}</p>
            </div>&nbsp;&nbsp;&nbsp;
            <div class="form-group row col-sm-3">
                <p class="mt-3" style=" color: rgb(10,10,10);">&nbsp;&nbsp;ปีการศึกษา</p>
                <p class="mt-3" style=" color: rgb(60,60,60);">&nbsp; {{$row->yearedu}}</p>
            </div>
           
            <div class="form-group text-right col-sm-4">
            <a href="{{ url('showcalandarexam/officer', $row->id) }}" class="btn btn-warning">ตารางสอบ</a>

                <a href="{{url('examsoff/poin', $row->id)}}" type="button" method="get" class="btn btn-info">ให้คะแนนสอบ</a>

            </div>
        </div>
    </div>
</div>
<br>
@endforeach

<hr>

@endsection