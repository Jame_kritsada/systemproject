@extends('Layout.masterstd')
@section('title')
@endsection('title')
@section('content')




<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">แก้ไขโปรเจ็ค</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{action('EditProjectController@update',$editproject->id)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="inputPassword4">รหัสระบบ</label>
                            <input type="text" class="form-control" name="education_id" value="{{$editproject->education_id}}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputPassword4">ระบบ</label>
                            <input type="text" class="form-control" name="education_name" value="{{$editproject->education_name}}">
                            </div>
                        </div>
                        <div class=" text-center">
                            <button type="submit" class="btn btn-primary " onclick="return confirm('ต้องการแก้ไขข้อมูลใช่ หรือ ไม่')">เพิ่มข้อมูล</button>
                        </div>
                        <input type="hidden" name="_method" value="PATCH">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


