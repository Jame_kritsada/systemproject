<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use App\Student;
use App\User;
use Illuminate\Support\Facades\Auth;

class RegistersController extends Controller
{
    public function index(Request $request)
    {

        $idu = Auth::user()->id;
        $Student = new Student();
        $Student->student_id = $request->input('student_id');
        $Student->student_name = $request->input('student_name');
        $Student->student_lastname = $request->input('student_lastname');
        $Student->tel_std = $request->input('tel_std');
        $Student->user_id = $idu;
        $Student->save();
        return view('page.student.menuproject');
    }
}
