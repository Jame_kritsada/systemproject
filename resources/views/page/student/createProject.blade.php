@extends('Layout.masterstd')
@section('title')
@endsection('title')
@section('content')
<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
    <h4>&nbsp;&nbsp;สร้างโปรเจค</h4>
</div>
<div class="container">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    <form action="{{ route('createProject.store') }}" method="post">
        {{ csrf_field() }}
        <div class="form">
            <input type="hidden" class="form-control" name="status_topic_adviser" value="รอการอนุมัติ">
            <input type="hidden" class="form-control" name="status_topic_admin" value="รอการอนุมัติ">
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="inputPassword4">โปรเจค</label>
                    <select class="form-control" name="pro1_pro2_status" required="">
                        <option selected value="">เลือก.Pro1,Pro2</option>
                        <option value="Project1">Project1</option>
                        <option value="Project1">Project2</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputPassword4">ประเภทโปรเจค</label>
                    <select class="form-control" name="projecttype_id" required="">
                        @foreach ($project_types as $p)
                        <option value="{{ $p->id }}">{{ $p->projecttype_detail }}

                        </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-5">
                    <label for="inputPassword4">สมาชิก</label>
                    <select class="form-control selectpicker" name="students_id[]" multiple data-live-search="true" required="">
                        @foreach ($students as $s)
                        <option value="{{ $s->id }}">{{ $s->student_name }}
                            {{ $s->student_lastname }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-5">
                    <label for="inputPassword4">ที่ปรึกษา</label>
                    <select name="officer_id[]" id="Xselect" class="form-control selectpicker" multiple data-live-search="true" required="">
                        @foreach ($officer as $s)
                        <option value="{{ $s->id }}">{{ $s->officer_name }} {{ $s->officer_lastname }}
                        </option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="row">
                <div class="form-group  col-md-5">
                    <label for="inputPassword4">ชื่อโปรเจค(ไทย)</label>
                    <input type="text" class="form-control" name="project_nameth" required="">
                </div>
                <div class="form-group  col-md-5">
                    <label for="inputPassword4">ชื่อโปรเจค(อังกฤษ)</label>
                    <input type="text" class="form-control" name="project_nameen" required="">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-8">
                    <label for="inputPassword10">รายละเอียดโปรเจค(ไทย)</label>
                    <textarea class="form-control" rows="7" id="comment" name="project_detailth" required=""></textarea>
                </div>
                <div class="form-group col-md-8">
                    <label for="inputPassword10">รายละเอียดโปรเจค(อังกฤษ)</label>
                    <textarea class="form-control" rows="7" id="comment" name="project_detailen" required=""></textarea>
                </div>
            </div>
        </div>

        <div class=" text-center">
            <button type="submit" class="btn btn-primary " onclick="return confirm('ต้องการสร้างใช่ หรือ ไม่')"><i class="bx bx-plus-circle">สร้างโปรเจค</i></button>
        </div>
    </form>
</div>







@endsection