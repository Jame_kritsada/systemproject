<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestExam extends Model
{
    protected $fillable = [
        'score_meetting',
        'score_beformidterm',
        'score_aftermidterm',
        'score_midterm',
        'score_final',
        'status_reqexm',
        'file_reqexm',
        'project_id',
        ];
      protected $primaryKey = 'id';
}
