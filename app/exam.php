<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class exam extends Model
{
    protected $fillable = [
        'topic',
        'typeproject',
        'yearedu',
        'dateexamstart',
        'dateexamend',
        'dateregisstart',
        'dateregisend',
        'detail',
        'status',

        ];
      protected $primaryKey = 'id';

}
