<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('register2', function () {
    return view('auth.register2');
});

Route::post('regis/insert', 'RegistersController@index');

//student
Route::get('filefull', function () {
    $idu = Auth::user()->id;
    $data['project'] = DB::table('membersprojects')
        ->select('projects_id')
        ->where('students_id', $idu)
        ->get();
    $data['fileproject'] = DB::table('membersprojects')
        ->leftJoin('projects', 'membersprojects.projects_id', 'projects.id')
        ->where('membersprojects.students_id', $idu)
        ->get();
    //dd($data['fileproject']);
    return view('page.student.upFileFull', $data);
});
Route::post('upfilefull/{id}', 'myProjectController@store');
Route::post('Requestexam/{id}', 'RequestexamController@index');
Route::post('Requestexams/addexam', 'RequestexamController@store');
Route::get('menu/project', function () {
    return view('page.student.menuproject');
});
Route::post('studentFileEnd/{id}', 'RequestexamController@updateFileEnd');
Route::post('upstatus_pro2/{id}', 'AdminController@upstatus_exam');

Route::get('exam/show', 'RequestexamController@showexam');
Route::get('exam/showcalendar', 'RequestexamController@showexamcalendar');

Route::get('exam/showstatus', 'RequestexamController@showstatus');

Route::resource('Projectall', 'ProjectallController');
Route::resource('stdinfor', 'StdinformationController');
Route::resource('adviser', 'AdviserController');
Route::resource('myProject', 'myProjectController');

Route::resource('editproject', 'EditProjectController');

Route::get('std1', function () {
    return view('page.student.std1');
});
Route::resource('createProject', 'createProjectController');

Route::get('std5', function () {
    return view('page.student.std5');
});

Route::get('studentHome', function () {
    return view('page.student.studentHome');
});



Route::get('/MidtermExam', function () {
    return view('page/student/MidtermExam');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//profesors
Route::post('upstatus_adviser/{project_nameth}', 'ConsultantRequestController@upstatus_adviser');
Route::get('detel_adviser/{project_nameth}', 'ConsultantRequestController@detel');
Route::resource('ConsultantRequest', 'ConsultantRequestController');
Route::post('exam/addpoin', 'examofficersController@addpoinofficer');

Route::post('upstatus_studentexam/{project_name}', 'StudentExamOfficerController@upstatus_adviser');
Route::get('detel_studentexam/{project_name}', 'StudentExamOfficerController@detel');
Route::get('examsoff/poin/{exam_id}', 'examofficersController@addpoin');
Route::get('examsoff/poin/show/{id}', 'examofficersController@showpoin');
Route::get('showcalandarexam/{id}', 'ExamController@showcalandarexam');
Route::get('showcalandarexam/officer/{id}', 'examofficersController@showcalandarexam');

Route::resource('StudentExamOfficer', 'StudentExamOfficerController');
Route::resource('examofficers', 'examofficersController');

Route::resource('ListProjectsConsult', 'ListProjectsConsultController');

Route::get('DocumentProjectConsultant', function () {
    return view('page.profesor.DocumentProjectConsultant');
});
Route::get('profesHome', function () {
    return view('page.profesor.profesHome');
});

Route::get('managemember', function () {
    return view('page.profesor.managemember');
});
Route::get('/', function () {
    return view('auth.login');
});
Route::resource('createExam', 'ExamController');
Route::resource('Exam', 'ExamController');




//admin 
Route::post('upstatus_studentexamAD/{project_name}', 'StudentExamAdminController@upstatus_adviser');
Route::get('detel_studentexamAD/{project_name}', 'StudentExamAdminController@detel');
Route::resource('StudentExamAdmin', 'StudentExamAdminController');

Route::post('upstatus_admin/{project_name}', 'ConsultantRequestAdminController@upstatus_admin');
Route::get('detel_admin/{project_name}', 'ConsultantRequestAdminController@detel');
Route::resource('ConsultantRequestAdmin', 'ConsultantRequestAdminController');
Route::patch('Examadminuppoin/{exam_id}', 'ExamadminController@update');

Route::get('detel/{project_name}', 'AdminController@detel');
Route::get('exam/showexam', 'ExamadminController@index');
Route::get('exam/manage/{id}', 'ExamadminController@manage');
Route::delete('exam/editmanage/{id}', 'ExamadminController@destroy');
Route::post('Exam/manage/create', 'ExamadminController@managecreate');
Route::get('examsadmin/poin/{id}', 'ExamadminController@addpoin');
Route::post('examadmin/addpoin', 'ExamadminController@addpoinadmin');
Route::get('examsadmin/poin/show/{id}', 'ExamadminController@showpoin');
Route::get('examsadmin/seccess/{id}', 'ExamadminController@seccess');
Route::get('examsadmin/report/{id}', 'ExamadminController@report');


Route::resource('requesteexam', 'AdminController');
Route::post('upstatus1/{project_name}', 'AdminController@upstatus1');

Route::resource('Adviser', 'AdviserController');

Route::get('/allproject', 'AdminController@allproject')->name('allproject');
Route::get('/allprojectAdmin', 'AdminController@allprojectAdmin')->name('allprojectAdmin');

Route::resource('officer', 'OfficerController');
Route::post('addofficer', 'OfficerController@store');
Route::resource('Addadviser', 'AdviserController');
Route::post('createAdviser', 'AdviserController@store');
Route::resource('project_type', 'ProjecttypesController');

Route::resource('requestexam2', 'RequestexamController');
Route::post('add/status/{id}', 'RequesteexamController@upstatus');
Route::resource('member', 'Member1Controller');

Route::delete('/destroy/{id}', 'Member1Controller@destroy');



Route::get('createmember', function () {
    return view('auth.register');
});
