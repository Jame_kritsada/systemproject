<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\project;
use Illuminate\Support\Facades\Hash;

class ListProjectsConsultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id =  Auth::user()->id;
        $ids = DB::table('officers')->select('id')
            ->where('user_id', '=', $id)->first();

        // $idc = DB::table('officer_groupprojecttypes')->select('id')
        //     ->where('officer_id', '=', $ids->id)->first();
        // dd($ids->id);


        $data['project'] = DB::table('advisers')
            ->join('projects', 'advisers.projects_id', 'projects.id')
            ->select(
                'projects.pro1_pro2_status',
                'projects.status_finished_notfinished',
                'projects.project_nameth'
            )->where('advisers.officer_id', $ids->id)
            ->where('projects.status_topic_admin' ,'=', 'ผ่านการอนุมัติ')
            ->get();
        // dd($data['advisers']);
        return view('page.profesor.ListProjectsConsult', $data)->with('success', 'บันทึกสำเร็จ');
    }
    public function upstatus(Request $request, $id)
    {

        $validatedData = $request->validate([]);
        $project = project::find($id);
        $project->status = $request->get('status');
        $project->save();
        return back()->with('success', 'บันทึกสำเร็จ');
    }
}
