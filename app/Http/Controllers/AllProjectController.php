<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\project;
use Illuminate\Support\Facades\Hash;

class AllProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['project'] = DB::table('projects')
        ->leftJoin('studentadexams','projects.id','studentadexams.project_id')
        ->select(
            'studentadexams.id as ids',
            'projects.id'
        )->get();
        return view('page.profesor.allproject', $data)->with('success', 'บันทึกสำเร็จ');
  
  
    }
    
}
