<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileexamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fileexams', function (Blueprint $table) {
            $table->increments('id');
            $table->string ('file_name');
            $table->integer('id_studentadexams')->unsigned();
            $table->timestamps();
            $table->foreign('id_studentadexams')->references('id')->on('studentadexams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fileexams');
    }
}
