<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\fileexam;
use App\studentadexam;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class RequestexamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, Request $request)
    {

        $data['exams'] = DB::table('exams')
            ->where(
                'id',
                '=',
                $id
            )->get();

        $examss = DB::table('exams')
            ->where(
                'id',
                '=',
                $id
            )->first();

        $id =  Auth::user()->id;

        $ids = DB::table('students')->select('id')
            ->where('user_id', '=', $id)->first();
        $data['projects'] = DB::table('membersprojects')
            ->join('projects', 'membersprojects.projects_id', 'projects.id')
            ->whereNotIn('projects_id', DB::table('studentadexams')->select('project_id'))
            ->where('status_topic_admin', '=', 'ผ่านการอนุมัติ')
            ->where('pro1_pro2_status', '=', $examss->typeproject)
            ->where('membersprojects.students_id', '=', $ids->id)
            ->get();
        // dd($data['projects']);
        return view('page.student.Requestexam', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function showexamcalendar()
    {
        $data['exam'] = DB::table('showexamcalendarview')
            // ->leftjoin('projects', 'studentadexams.project_id', 'projects.id')
            // ->leftjoin('officer_exams', 'showexamcalendarview.id', 'officer_exams.studentadexams_id')
            ->where('status_exam_admin', '=', 'ผ่านการอนุมัติ')
            ->where('status','!=','ปิดใช้งาน')
            ->get();
        // dd($data['exam']);
        return view('page.student.examshowcalendar', $data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('file_name') != '') {

            $file = $request->file('file_name');
            $ext = $file->getClientOriginalExtension();

            $name = md5(rand() * time()) . '.' . $ext;
            $file->move(public_path('fileexam'), $name);
        } else {
            $name = '';
        }

        $studentadexam = new studentadexam();
        $studentadexam->status_exam_adviser = $request->input('status_exam_adviser');
        $studentadexam->status_exam_admin = $request->input('status_exam_admin');
        $studentadexam->project_id = $request->input('project_id');
        $studentadexam->filefirst =  $name;
        $studentadexam->exam_id = $request->input('exam_id');
        $studentadexam->save();
        $examproject_id = $studentadexam->id;
        // dd($request->file('file_name'));
        // if ($request->file('file_name') != '') {

        //     $file = $request->file('file_name');
        //     $ext = $file->getClientOriginalExtension();

        //     $name = md5(rand() * time()) . '.' . $ext;
        //     $file->move(public_path('fileexam'), $name);
        // } else {
        //     $name = '';
        // }

        // $fileexam = new fileexam();
        // $fileexam->file_name = $name;
        // $fileexam->id_studentadexams = $examproject_id;
        // $fileexam->save();
        return redirect(url('exam/show'));
    }
    public function updateFileEnd(Request $request, $id)
    {
        //dd($request);
        if ($request->file('file_name') != '') {

            $file = $request->file('file_name');
            $ext = $file->getClientOriginalExtension();

            $name = md5(rand() * time()) . '.' . $ext;
            $file->move(public_path('fileend'), $name);
        } else {
            $name = '';
        }
        //dd($name);
        $studentadexam = studentadexam::find($id);
        $studentadexam->fileend =  $name;
        //dd($studentadexam);
        $studentadexam->save();
        return  back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showstatus()
    {


        // $idu = Auth::user()->id;
        // $ids = DB::table('students')->select('id')
        //     ->where('user_id', '=', $idu)->first();

        // $data['studentadexams'] = DB::table('membersprojects')
        //     ->join('projects', 'membersprojects.projects_id', 'projects.id')
        //     ->select(
        //         'projects.id',
        //         'projects.project_nameth',
        //         'projects.status_topic_adviser',
        //         'projects.status_topic_admin',
        //         'projects.pro1_pro2_status',
        //         'projects.created_at'
        //     )->where('membersprojects.students_id', '=', $ids->id)->get();



        // dd($data['studentadexams']);

        $idu = Auth::user()->id;
        $ids = DB::table('students')->select('id')
            ->where('user_id', '=', $idu)->first();

        $data['statusexams'] = DB::table('memberpro')
            ->join('statusexam', 'memberpro.project_id', 'statusexam.project_id')
            ->select(
                'statusexam.project_id as id',
                'memberpro.project_nameth',
                'memberpro.project_nameen',
                'statusexam.topic',
                'statusexam.status_exam_admin',
                'statusexam.dateexamstart',
                'statusexam.dateexamend'

            )->where('student_id', '=', $ids->id)->get();
        // dd($data['statusexams']);
        return view('page.student.examshowstatus', $data);
    }
    public function showexam(request $request)
    {
        $idu = Auth::user()->id;
        $ids = DB::table('students')->select('id')
            ->where('user_id', '=', $idu)->first();

        // dd($ids);
        // $officers = officer::find($ids->id);
        // $officers->delete();



        $data['membersprojects'] = DB::table('membersprojects')
            ->join('projects', 'membersprojects.projects_id', 'projects.id')
            ->select(
                'projects.id',
                'projects.project_nameth',
                'projects.project_nameen',
                'projects.status_topic_adviser',
                'projects.status_topic_admin',
                'projects.pro1_pro2_status',
                'projects.created_at'
            )->where('membersprojects.students_id', '=', $ids->id)->get();
        $now = now();
        $data['exams'] = DB::table('exams')
            ->select(
                'id',
                'topic',
                'typeproject',
                'dateregisstart',
                'dateregisend',

                'status'
            )->where('status', 'ใช้งาน')
            ->where('dateregisstart', '<=', now())
            ->where('dateregisend', '>=', now())
            ->get();

        //  dd($data['exams']);
        return view('page.student.exam', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['exams'] = DB::table('exams')
            ->where(
                'id',
                '=',
                $id
            )->get();
        $id =  Auth::user()->id;

        $ids = DB::table('students')->select('id')
            ->where('user_id', '=', $id)->first();
        $data['projects'] = DB::table('membersprojects')
            ->join('projects', 'membersprojects.projects_id', 'projects.id')
            ->where('status_topic_admin', '=', 'ผ่านการอนุมัติ')
            ->where('membersprojects.students_id', '=', $ids->id)
            ->get();

        // dd($data['projects']);
        return view('page.student.EditRequestexam', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $studentadexam = studentadexam::find($id);
        $studentadexam->status_exam_adviser = $request->input('status_exam_adviser');
        $studentadexam->status_exam_admin = $request->input('status_exam_admin');
        $studentadexam->project_id = $request->input('project_id');
        $studentadexam->exam_id = $request->input('exam_id');
        $studentadexam->save();
        $examproject_id = $studentadexam->id;
        // dd($request->file('file_name'));
        if ($request->file('file_name') != '') {

            $file = $request->file('file_name');
            $ext = $file->getClientOriginalExtension();

            $name = md5(rand() * time()) . '.' . $ext;
            $file->move(public_path('fileexam'), $name);
        } else {
            $name = '';
        }

        $fileexam = new fileexam();
        $fileexam->file_name = $name;
        $fileexam->id_studentadexams = $examproject_id;
        $fileexam->save();
        return redirect(url('exam/show'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deletestudentadexam = studentadexam::find($id);
        $deletestudentadexam->delete();
        return back();
    }
}
