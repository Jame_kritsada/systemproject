<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project_type;
use App\officer_groupprojecttype;
use App\User;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;

class ProjecttypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['project_type'] = DB::table('project_types')->get();
        // dd($data);
        $data['officers'] = DB::table('officers')
            ->get();
        return view('page.admin.addproject_type', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $project_type = new project_type;
        $project_type->projecttype_name = $request->input('projecttype_name');
        $project_type->projecttype_detail = $request->input('projecttype_detail');
        $project_type->save();
        
        $id = $project_type->id;
        $officer = $request->input('officer_id');
        $size = count(collect($officer));
// dd($request->input('officer_id'));
        for ($i = 0; $i < $size; $i++) {
            $officer1 = new officer_groupprojecttype;
            $officer1->projecttype_id = $id;
            $officer1->officer_id =  $officer[$i];
            $officer1->save();
        }


        // foreach ($request->officer_id as $val) {

        //     if (isset($val['officer_id'])) {

        //         $officer = new officer_groupprojecttype;
        //         $officer->projecttype_id = $id;
        //         $officer->officer_id =  $val['officer_id'];
        //         $officer->save();
        //         dd($id);
        //     }
        // }
        // }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['project_type'] = project_type::find($id);
        // dd($data);
        return view('page.admin.editproject_type', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([]);
        $project_type = project_type::find($id);
        $project_type->projecttype_name = $request->get('projecttype_name');
        $project_type->save();
        return redirect()->route('project_type.index')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project_type = project_type::find($id);
        $project_type->delete();
        return redirect()->route('project_type.index')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
