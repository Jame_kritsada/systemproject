<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->increments('id');
            $table->string ('topic');
            $table->string ('typeproject');
            $table->string ('yearedu');
            $table->date ('dateexamstart');
            $table->date ('dateexamend');
            $table->date ('dateregisstart');
            $table->date ('dateregisend');
            $table->string ('detail');
            $table->string ('status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
