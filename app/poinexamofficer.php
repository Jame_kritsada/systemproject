<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class poinexamofficer extends Model
{
    protected $fillable = [
        'poin',
        'id_studentadexams',
        'id_user',
        ];
      protected $primaryKey = 'id';
}
