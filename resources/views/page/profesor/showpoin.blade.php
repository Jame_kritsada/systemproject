@extends('Layout.master')
@section('title')
สอบ
@endsection('title')

@section('content')

<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>

</div>

<div class="table-responsive">
    <table class="table  tabel-bordered  table-striped">
        <tr>
            <th>การสอบ</th>
            <th>โปรเจ็ค</th>
            <th>วิชา</th>
            <th>คะแนน</th>
        </tr>
        @foreach ($exams as $row)
        <tr>
            <td>@if($row->topic == 'midterm')กลางภาค
                @elseif($row->topic == 'final')ปลายภาค
                @endif
            </td>
            <td>{{ $row->project_nameth }}</td>
            <td>{{ $row->typeproject }}</td>
            <td>{{ $row->poin }}</td>

        </tr>
        @endforeach
    </table>
</div>


@endsection