<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficerExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('officer_exams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_nameth');
            $table->date('date');

            $table->integer('officer_id')->unsigned();
            $table->integer('exam_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('studentadexams_id')->unsigned();

            $table->text('detail');

            $table->timestamps();
            $table->foreign('officer_id')->references('id')->on('officers')->onDelete('cascade');
            $table->foreign('exam_id')->references('id')->on('exams')->onDelete('cascade');
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('studentadexams_id')->references('id')->on('studentadexams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('officer_exams');
    }
}
