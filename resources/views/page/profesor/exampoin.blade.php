@extends('Layout.master')
@section('title')
สอบ
@endsection('title')

@section('content')

<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
    @foreach ($examss as $a)
    <a href="{{url('examsoff/poin/show', $a->id)}}" class="btn btn-info">โปรเจคที่ให้คะแนนแล้ว</a>
    @endforeach
</div>
@foreach ($exams as $row)
<div class="card mt-3">
    <div class="card-body shadow">

        <div class="form-row col-12">

            <div class="form-group row col-10">
                <p style=" color: rgb(10,10,10);">ชื่อโปรเจค {{$row ->project_nameth}}</p>
            </div>
        </div>
        <div class="form-group row col-5">
            <p style=" color: rgb(10,10,10);">วัน/เวลาสอบ : {{$row ->date}} {{$row ->detail}}</p>
        </div>
        <div class="form-group row col-5">
            <p style=" color: rgb(10,10,10);">ไฟล์แนบ</p>
            <a href="{{asset('fileexam/'.$row->filefirst)}}" download style=" color: rgb(60,60,60);  margin-left:2%;">ดาวน์โหลดไฟล์</a>
        </div>


        <form action="{{ url('exam/addpoin') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="hidden" name="studentadexams_id" value="{{$row ->studentadexams_id}}">
                <input type="hidden" name="project_id" value="{{$row ->project_id}}">
                <input type="hidden" name="project_nameth" value="{{$row ->project_nameth}}">
                <input type="hidden" name="exam_id" value="{{$row ->exam_id}}">

                <input type="text" name="poin" class="form-control col-2 " placeholder="กรอกคะแนน">
            </div>
            <textarea type="text" name="comment" class="form-control col-10 "></textarea>

            <button type="submit" class="btn btn-info col-1 mt-2">บันทึก</button>

        </form>

        <div class="form-group row col-1">


        </div>


    </div>
</div>

@endforeach

<hr>
@endsection