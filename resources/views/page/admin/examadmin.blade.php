@extends('Layout.master')
@section('title')
สอบ
@endsection('title')

@section('content')

<div class="section-title">
    <h2>สอบ</h2>
</div>
@foreach ($exams as $row)
<div class="card">
    <div class="card-body shadow">
        <dt>รายละเอียด</dt>
        <div class="form-row col-12">
            <div class="form-group col-5">
                @if($row->topic == 'midterm')
                <p class="mt-3" style=" color: rgb(60,60,60);">&nbsp;&nbsp;สอบกลางภาค</p>
                @elseif($row->topic == 'final')
                <p class="mt-3" style=" color:  rgb(60,60,60);">&nbsp;&nbsp;สอบปลายภาค</p>
                @endif
                <p class="mt-3" style=" color: rgb(60,60,60);">&nbsp;&nbsp;{{$row->typeproject}}</p>
                <p class="mt-3" style=" color: rgb(60,60,60);">&nbsp;&nbsp;{{$row->dateexamstart}}&nbsp;ถึง&nbsp;{{$row->dateexamend}}</p>
            </div>
            <div class="form-group row col-3">
                <p class="mt-3" style=" color: rgb(10,10,10);">&nbsp;&nbsp;ปีการศึกษา</p>
                <p class="mt-3" style=" color: rgb(60,60,60);">&nbsp; {{$row->yearedu}}</p>
            </div>
            <div class="form-group text-right col-4">
                <a href="{{url('examsadmin/poin', $row->id)}}" type="button" method="get" class="btn btn-info">ให้คะแนนสอบ</a>
                <a href="{{url('examsadmin/seccess', $row->id)}}" type="button" method="get" class="btn btn-info">สิ้นสุดการสอบ</a>

            </div>
        </div>
    </div>
</div>
<br>
@endforeach

<hr>
@endsection