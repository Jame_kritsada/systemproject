<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class EditProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['editproject']= DB::table('projects')->get();
        return view('education.addeducation',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'education_id' => 'required',
            'education_name' => 'required',
            
        ]);
        if (!$validator->fails()) {
            $Project = new Project;
            $Project ->education_id = $request->input('education_id');
            $Project ->education_name = $request->input('education_name');
            $Project ->save();

            return redirect()->route('education.create')->with('success', 'บันทึกสำเร็จ');
        } else {
            return redirect()->route('education.create')->with('warning', 'บันทึกไม่สำเร็จ');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['editproject'] = Project::find($id);
        $data['comentadviser'] = DB::table('comentadvisers')
            ->join('projects', 'comentadvisers.id_project', 'projects.id')
            ->select(
                'comentadvisers.id',
                'comentadvisers.detail',
            )
            ->where('comentadvisers.id', '=', $id)
            ->get();
         
        $data['comentadmin'] = DB::table('comentadmins')
            ->join('projects', 'comentadmins.id_project', 'projects.id')
            ->select(
                'comentadmins.id',
                'comentadmins.detail',
            )
            ->where('comentadmins.id', '=', $id)
            ->get();
            // dd( $data['comentadmin']);
        // dd($data);
        return view('page.student.EditProject', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
                  
            ]);
            $Project = Project::find($id);
            $Project ->education_id = $request->input('education_id');
            $Project ->education_name = $request->get('education_name');
            $Project->save();
            return redirect()->route('education.create')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project =Project::find($id);
        $project->delete();
        return redirect()->route('education.create')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
