<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log In Page</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
        integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
        crossorigin="anonymous" />
    <meta charset="UTF-8">
    <title>ระบบจัดการทุนการศึกษา</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @yield('third_party_stylesheets')

    @stack('page_css')
</head>

<style>
    body {
        background-image: url('img/bg.jpg');
        background-color: #cccccc;
        background-size: 100%;

    }

    h1 {
        text-shadow: 1px 1px #ffffff;
    }


</style>

<body>

    <nav class="navbar sticky-top navbar-expand-xl navbar-warning  bg-warning  mb-5">
        <a class="navbar-brand" href="{{ url('/home') }}"></a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-nav"
            aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="main-nav" style="">
            @if (Route::has('login'))
                <ul class="nav navbar-nav ml-auto">
                    @auth
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/home') }}" rel="nofollow">Home</a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="btn nav-link" href="{{ route('login') }}" rel="nofollow"> <i
                                    class="fas fa-sign-in-alt"></i> เข้าสู่ระบบ</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="btn nav-link" href="{{ route('register') }}"> <i class="fas fa-user-plus"></i>
                                    สมัครสมาชิก</a>
                            </li>
                        @endif
                    @endauth
                </ul>
            @endif
        </div>
    </nav>

    <div class="row">
        <div class="col-md-1 mt-6"></div>
        <div class="col-md-6 col-lg-3 col-sm-8 mt-5">
            <div class="card card-secondary card-outline" style="height: 610px">
                <div class="card" style="height: 610px">
                    <div class="login-logo mt-4">
                        <a href="{{ url('/home') }}"><b>ระบบทุนการศึกษา</b><br>
                            <h5>มหาวิทยาลัยมหาสารคาม</h5>
                        </a>
                    </div>
                    <!-- /.login-logo -->

                    <!-- /.login-box-body -->

                    <div class="card-body login-card-body">
                        <p class="login-box-msg">เข้าสู่ระบบ</p>

                        <form method="post" action="{{ url('/login') }}">
                            @csrf

                            <div class="input-group mb-4">
                                <input type="email" name="email" value="{{ old('email') }}" placeholder="Email"
                                    class="form-control @error('email') is-invalid @enderror">
                                <div class="input-group-append">
                                    <div class="input-group-text"><span class="fas fa-envelope"></span></div>
                                </div>
                                @error('email')
                                    <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="input-group mb-4">
                                <input type="password" name="password" placeholder="Password"
                                    class="form-control @error('password') is-invalid @enderror">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                                @error('password')
                                    <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror

                            </div>

                            <div class="row">
                               

                                <div class="col-4 mt-4 ">
                                    <button type="submit" class="btn btn-primary btn-block">เข้าสู่ระบบ</button>
                                </div>

                            </div>
                        </form>

                       
                    </div>
                    <!-- /.login-card-body -->
                </div>

            </div>
            <!-- /.login-box -->
        </div>
        <div class="col-lg-8 mt-6">
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="text-md-center mt-5 ">
                 <img src="{{ asset('img/logo_msu.png') }}" alt="img_logo" width="10%" height="10%">
            </div>
            <div class="text-md-center mt-3">
                <h1 style="">กองกิจการนิสิต</h1>

                <h1> มหาวิทยาลัยมหาสารคาม </h1>
            </div>

        </div>
    </div>

   
    <script src="{{ mix('js/app.js') }}" defer></script>

</body>

</html>
