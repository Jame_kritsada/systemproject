<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class studentadexam extends Model
{
    protected $fillable = [
    
        'status_exam_adviser',
        'status_exam_admin',
        'project_id',
        'exam_id',
       
      ];
      protected $primaryKey = 'id';
    }
