<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\comentadviser;
use App\studentadexam;
use Illuminate\Support\Facades\Hash;

class StudentExamOfficerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $id =  Auth::user()->id;
        $ids = DB::table('officers')->select('id')
            ->where('user_id', '=', $id)->first();

       
        // dd($ids->id);


        $data['studentexamofficer'] = DB::table('studentadexams')
            ->join('projects', 'studentadexams.project_id', 'projects.id')
            ->join('exams', 'studentadexams.exam_id', 'exams.id')
            ->join('idoffiergroup', 'studentadexams.project_id', 'idoffiergroup.project_id')
            ->select(
                'projects.project_nameth',
                'projects.project_nameen',
                'projects.project_detailth',
                'projects.project_detailen',
                'exams.typeproject',
                'exams.yearedu',
                'exams.detail',
                'exams.status',
                'studentadexams.id',
                'studentadexams.status_exam_adviser',
                'studentadexams.status_exam_admin'
            )->where('idoffiergroup.id_officergroup', $ids->id)
            ->where('studentadexams.status_exam_adviser', '=', 'รอการอนุมัติ')
            ->get();
    //  dd($data['studentexamofficer']);
        return view('page.profesor.StudentExamOfficer', $data)->with('success', 'บันทึกสำเร็จ');
    }

    public function detel(Request $request, $project_name)
    {

        $ids = DB::table('projects')->select('id')
            ->where('project_nameth', '=', $project_name)->first();

        $data['projectss'] = DB::table('projects')
            ->where('project_nameth', '=', $project_name)
            ->get();

        $data['student'] = DB::table('membersprojects')
            ->join('students', 'membersprojects.students_id', 'students.id')

            ->where('membersprojects.projects_id', '=', $ids->id)
            // ->groupBy('membersprojects.students_id')
            ->get();
        //  dd( $data['student']);
        return view('page.profesor.detelStudentExamOfficer', $data);
    }

    public function upstatus_adviser(Request $request, $id)
    {
        //   dd($project_name);
        

        // $ids = DB::table('projects')->select('id')
        //     ->where('project_name', '=', $project_name)->first();
        //   dd($ids);
        if ($request->get('status_exam_adviser') == 'ไม่ผ่านการอนุมัติ') {

            $validatedData = $request->validate([]);
            $status_exam_adviser = studentadexam::find($id);
            $status_exam_adviser->status_exam_adviser = $request->get('status_exam_adviser');
            $status_exam_adviser->save();

            // $comentadvisers->save();
        } elseif (($request->get('status_exam_adviser') == 'ผ่านการอนุมัติ')) {
            $validatedData = $request->validate([]);
            $status_exam_adviser = studentadexam::find($id);
            $status_exam_adviser->status_exam_adviser = $request->get('status_exam_adviser');
            $status_exam_adviser->save();
        }
        return back()->with('success', 'บันทึกสำเร็จ');
    }
}
