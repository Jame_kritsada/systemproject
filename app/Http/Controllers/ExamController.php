<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Project;
use App\exam;
use App\officer_exam;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['addexam'] = DB::table('exams')
        // ->join('exams','officer_exams.exam_id','exams.id')
        ->select(  
        )->orderBy('created_at','Desc')
        ->get();
       
// dd($data['addexam']);
        return view('page.admin.CreateExam',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        

        $Exam = new exam();
        $Exam->topic = $request->input('topic');
        $Exam->typeproject = $request->input('typeproject');
        $Exam->yearedu = $request->input('yearedu');
        $Exam->dateexamstart = $request->input('dateexamstart');
        $Exam->dateexamend = $request->input('dateexamend');
        $Exam->dateregisstart = $request->input('dateregisstart');
        $Exam->status = $request->input('status');
        $Exam->dateregisend = $request->input('dateregisend');
        $Exam->detail = $request->input('detail');
        $Exam->save();

        return back()->with('success', 'บันทึกสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['editexam'] = DB::table('officer_exams')
        ->join('exams','officer_exams.exam_id','exams.id')
        ->join('officers','officer_exams.officer_id','officers.id')
        ->select(
            'officer_exams.id',
            'exams.topic',
            'exams.typeproject',
            'exams.yearedu',
            'exams.dateexamstart',
            'exams.dateexamend',
            'exams.dateregisstart',
            'exams.dateregisend',
            'exams.detail',
            'officers.officer_name',
            'officers.officer_lastname',
        )
        ->where('officer_exams.id',$id)
        ->get();
//dd($data);
        return view('page.admin.EditExam',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([]);

        $Exam = exam::find($id);
        $Exam->topic = $request->get('topic');
        $Exam->typeproject = $request->get('typeproject');
        $Exam->yearedu = $request->get('yearedu');
        $Exam->dateexamstart = $request->get('dateexamstart');
        $Exam->dateexamend = $request->get('dateexamend');
        $Exam->dateregisstart = $request->get('dateregisstart');
        $Exam->dateregisend = $request->get('dateregisend');
        $Exam->detail = $request->get('detail');
        $Exam->save();

        $id = $Exam->id;

        $officer = $request->get('officer');
        $size = count(collect($officer));

        for ($i = 0; $i < $size; $i++) {
            $student1 = officer_exam::find($id);
            $student1->exam_id = $id;
            $student1->officer_id =  $officer[$i];
            $student1->save();
        }

       

        // dd($user);
        return redirect()->route('Exam.index')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $officer_exams = exam::find($id);
        $officer_exams->delete();
        return redirect()->route('Exam.index')->with('success', 'ลบข้อมูลสำเร็จ');
    }

          
    public function upstatus_adviser(Request $request, $project_name)
    {
         //   dd($project_name);
         $ids = DB::table('projects')->select('id')
         ->where('project_name', '=', $project_name)->first();
     //   dd($ids);

        $validatedData = $request->validate([]);
        $projects = Project::find($ids->id);
        $projects->status_midterm = $request->get('status_midterm');
        $projects->save();
        return back()->with('success', 'บันทึกสำเร็จ');
    }
    public function showcalandarexam($id)
    {
        $data['showcalandarexam'] = DB::table('studentadexams')
        ->leftJoin('exams','studentadexams.exam_id','exams.id')
        ->leftJoin('projects','studentadexams.project_id','projects.id')
        ->where('studentadexams.exam_id',$id)
        ->get();
        // dd($data);
        return view('page.admin.showcalandarexam',$data);
    }
    
}
