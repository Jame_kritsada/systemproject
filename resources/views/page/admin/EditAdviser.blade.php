@extends('Layout.master')
@section('title')
    แก้ไขข้อมูลอาจารย์
@endsection('title')

@section('content')


    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">เพิ่มข้อมูล อาจารย์</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ action('AdviserController@update', $adviser->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">รหัสอาจารย์</label>
                                    <input type="text" class="form-control" name="adviser_id"
                                        value="{{ $adviser->adviser_id }}" pattern="[0-9]{1,}" title="กรอกตัวเลขเท่านั้น"
                                        required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">ชื่อ</label>
                                    <input type="text" class="form-control" name="adviser_name"
                                        value="{{ $adviser->adviser_name }}" pattern="[A-Za-zก-ฮ]{1,}"
                                        title="กรอกตัวอักษรเท่านั้น" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">สกุล</label>
                                    <input type="text" class="form-control" name="adviser_lastname"
                                        value="{{ $adviser->adviser_lastname }}" pattern="[0-9]{1,}"
                                        title="กรอกตัวเลขเท่านั้น" required>
                                </div>


                            </div>
                            <div class=" text-center">
                                <button type="submit" class="btn btn-primary "
                                    onclick="return confirm('ต้องการแก้ไขข้อมูลใช่ หรือ ไม่')">เพิ่มข้อมูล</button>
                            </div>
                            <input type="hidden" name="_method" value="PATCH">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
