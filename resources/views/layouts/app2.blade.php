<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log In Page</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
    <meta charset="UTF-8">
    <title>ระบบจัดการทุนการศึกษา</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @yield('third_party_stylesheets')

    @stack('page_css')
</head>
<style>
    body {
        background-image: url('img/02.jpg');
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: 100% 100%;
        backdrop-filter: blur(5px);
        background-color: rgba(255, 255, 255, 0.4);
    }
    h1 {
        text-shadow: 1px 1px #ffffff;
    }
</style>
<body>

    <nav class="navbar sticky-top navbar-expand-xl navbar-warning  bg-light shadow  mb-5">
    <a style="margin-left: 100px;" class="navbar-brand">INFORMATION TECHNOLOGY</a>
        <a class="navbar-brand" href="{{ url('/') }}"></a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
    </nav>

        @yield('content')
  
</body>
<script src="{{ mix('js/app.js') }}" defer></script>

</html>