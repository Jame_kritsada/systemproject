<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use App\Officer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
class AdviserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idu = Auth::user()->id;

        $data['officer'] = DB::table('officers')
            ->join('Users', 'officers.user_id', 'Users.id')
            ->select(
                'Users.id',
                'Users.email',
                'officers.officer_name',
                'officers.officer_lastname',
                'officers.tel_off'
                // )->orderBy('officers.of_name', 'ASC')
            )->where('Users.id','=',$idu)->get();

        return view('page.profesor.adviserinfor', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idu = Auth::user()->id;
        $data['officer'] = DB::table('officers')
            ->join('Users', 'officers.user_id', 'Users.id')
            ->select(
                'Users.id',
                'Users.email',
                'officers.officer_name',
                'officers.officer_lastname',
                'officers.tel_off'
                // )->orderBy('officers.of_name', 'ASC')
            )->where('Users.id','=',$idu)->get();

        return view('page.profesor.Editadviser', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    //    dd($id);
               
        
            $xxx = Officer::find($id);
            $xxx->officer_name = $request->input('officer_name');
            $xxx->officer_lastname = $request->input('officer_lastname');
            $xxx->tel_off = $request->input('tel_off');
            $xxx->save();
            return redirect()->route('adviser.index')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
