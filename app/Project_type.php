<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project_type extends Model
{
    protected $fillable = [
        'projecttype_name',
        'projecttype_detail',
        ];
      protected $primaryKey = 'id';
}
 