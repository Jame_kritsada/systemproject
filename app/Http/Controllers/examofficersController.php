<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Project;
use App\exam;
use App\poinexamofficer;
use App\officer_exam;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class examofficersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idu = Auth::user()->id;
        $ids = DB::table('officers')->select('id')
            ->where('user_id', '=', $idu)->first();

        $data['exams'] = DB::table('exams')
            ->select()
            ->where('status', '!=', 'ปิดใช้งาน')
            ->get();
        //  dd($data['exams']);
        return view('page.profesor.exam', $data);
    }

    public function showcalandarexam($id)
    {
        $data['showcalandarexam'] = DB::table('studentadexams')
        ->leftJoin('exams','studentadexams.exam_id','exams.id')
        ->leftJoin('projects','studentadexams.project_id','projects.id')
        ->where('studentadexams.exam_id',$id)
        ->get();
        // dd($data);
        return view('page.profesor.showcalandarexam',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addpoin($exam_id)
    {

        $idu = Auth::user()->id;
        $ids = DB::table('officers')->select('id')
            ->where('user_id', '=', $idu)->first();

        $data['exams'] = DB::table('officer_exams')
            ->join('studentadexams', 'officer_exams.studentadexams_id', 'studentadexams.id')
            ->select()
            ->whereNotIn('studentadexams_id', DB::table('poinexamofficers')->select('studentadexams_id')->where('id_user', $idu))
            ->where('officer_exams.exam_id', '=', $exam_id)
            ->where('officer_exams.officer_id', '=', $ids->id)
            ->get();
        $data['examss'] = DB::table('exams')
            ->select()
            ->where('id', '=', $exam_id)
            ->get();



        //  dd($data['exams']);
        return view('page.profesor.exampoin', $data);
    }
    public function showpoin($id)
    {

        $idu = Auth::user()->id;

        $data['exams'] = DB::table('poinexamofficers')
            ->leftJoin('exams', 'poinexamofficers.exam_id', 'exams.id')
            ->select()
            ->where('poinexamofficers.exam_id', '=', $id)
            ->where('poinexamofficers.id_user', '=', $idu)
            ->get();



        return view('page.profesor.showpoin', $data);
    }
    public function addpoinofficer(Request $request)
    {
        $idu = Auth::user()->id;
        $officer_name  = DB::table('officers')->select('officer_name')
            ->where('user_id', '=', $idu)->first();
        $poinexamofficer = new poinexamofficer();
        $poinexamofficer->poin = $request->input('poin');
        $poinexamofficer->comment = $request->input('comment');
        $poinexamofficer->officer_name = $officer_name->officer_name;
        $poinexamofficer->exam_id = $request->input('exam_id');
        $poinexamofficer->project_nameth = $request->input('project_nameth');

        $poinexamofficer->studentadexams_id = $request->input('studentadexams_id');
        $poinexamofficer->id_user = $idu;
        $poinexamofficer->save();
        return back();
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
