@extends('Layout.master')
@section('title')
@endsection('title')
@section('content')

<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
</div>
@foreach ($addexam as $row)
@if($row->topic == 'midterm')
<p style="font-size: 30px;"><i class="far fa-check-circle" style="font-size: 25px;  color:rgb(120,132,232);"></i> สอบกลางภาค</p>
@elseif($row->topic == 'final')
<p style="font-size: 30px;"><i class="far fa-check-circle" style="font-size: 25px;  color:rgb(120,132,232);"></i> สอบปลายภาค</p>
@endif
@if($row->typeproject == 'Project1')
<div class="row" style="margin-left:0%;">
    <p style="font-size: 20px; "><i class="fas fa-book" style="font-size: 25px;  color:rgb(120,132,232);"></i> รายวิชา : โปรเจค1</p>
    <p style="font-size: 20px; margin-left:1%;"><i class="far fa-calendar-alt" style="font-size: 25px;  color:rgb(120,132,232);"></i> ปีการศึกษา : {{$row->yearedu}}</p>
</div>
@elseif($row->typeproject == 'Project2')
<div class="row" style="margin-left:0%;">
    <p style="font-size: 20px;"><i class="fas fa-book" style="font-size: 25px;  color:rgb(120,132,232);"></i> รายวิชา : โปรเจค2</p>
    <p style="font-size: 20px; margin-left:1%;"><i class="far fa-calendar-alt" style="font-size: 25px;  color:rgb(120,132,232);"></i> ปีการศึกษา : {{$row->yearedu}}</p>
</div>
@endif
<p style="font-size: 20px;"><i class="far fa-calendar-alt" style="font-size: 25px;  color:rgb(120,132,232);"></i> ช่วงการสอบ : {{$row->dateexamstart}} ถึง {{$row->dateexamstart}}</p>
<p style="font-size: 20px;"><i class="fas fa-user-friends" style="font-size: 25px;  color:rgb(120,132,232);"></i> เลือกโปรเจคและอาจารย์คุมสอบ </p> <br>
<form action="{{ url('Exam/manage/create') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="exam_id" value="{{$row->id}}">
    <div class="col-12 container2">
        <div class="col-7">
            <p><i class="fas fa-align-center" style="font-size: 18px;  color:rgb(120,132,232);"></i> : โปรเจคที่คุมสอบ</p>
            <select class="custom " id="inputGroupSelect01" name="project_id">
                @foreach ($studentadexams as $ro)
                <option selected value="{{$ro->project_id}}">{{$ro->project_nameth}}</option>
                @endforeach
            </select><br><br>

            <hr>
            <p><i class="fas fa-users" style="font-size: 18px;  color:rgb(120,132,232);"></i> : อาจารย์คุมสอบ</p>
            <select name="officer_id[]" class="form-control selectpicker" multiple data-live-search="true" required="">
                @foreach ($officers as $s)
                <option value="{{ $s->id }}">{{ $s->officer_name }} {{ $s->officer_lastname }}
                </option>
                @endforeach
            </select>
            <br><br>
            <input class="form-control" name="date" type="date" min="<?php echo date('Y-m-d'); ?>"  id="example-date-input">
            <br>
            <input type="text" class="form-control" name="detail" placeholder="เวลา">
        </div><br>
    </div><br>


    <!-- <div class="form-row">
        <button class="btn btn-success add_form_field2" type="button"><i class="fa fa-plus-square"></i>
            เพิ่ม</button>
    </div><br> -->


    @endforeach

    <div class=" text-center">
        <button type="submit" class="btn btn-primary " onclick="return confirm('ต้องการเพิ่มข้อมูลใช่ หรือ ไม่')"><i class="bx bx-plus-circle">เพิ่มข้อมูล</i></button>
    </div>
</form>


<div class="table-responsive mt-5">
    <table class="table  tabel-bordered  table-striped">
        <tr>
            <th>ชื่อโปรเจค</th>
            <th>อาจารย์คุมสอบ</th>
            <th>รายละเอียด</th>
            <th>แก้ไข</th>
            <th>ลบ</th>
        </tr>
        @foreach ($officer_exams as $r)
        <tr>
            <td>{{ $r->project_nameth }}</td>
            <td> <?php
                    $con = mysqli_connect("127.0.0.1", "root", "", "systemproject");
                    // Check connection
                    if (mysqli_connect_errno()) {
                        echo "Failed to connect to MySQL: " . mysqli_connect_error();
                    }
                    $id = $r->project_id;
                    $result = mysqli_query($con, "SELECT * FROM officer_examsview WHERE project_id = $id ");



                    while ($row = mysqli_fetch_array($result)) {

                    ?>

                    <div class="mb-3">
                        <?php echo  $row['officer_name']; ?>
                    </div>

                <?php  }


                ?>
            </td>

            <td> <?php
                    $con = mysqli_connect("127.0.0.1", "root", "", "systemproject");
                    // Check connection
                    if (mysqli_connect_errno()) {
                        echo "Failed to connect to MySQL: " . mysqli_connect_error();
                    }
                    $id = $r->project_id;
                    $result = mysqli_query($con, "SELECT * FROM officer_examsview  WHERE project_id = $id GROUP BY project_id ");



                    while ($row = mysqli_fetch_array($result)) {

                    ?>

                    <div class="mb-3">
                        <?php echo  $row['detail']; ?>
                    </div>

                <?php  }


                ?>
            </td>
            <td>
                <a class="btn btn-warning" data-target="#modal-edit-customers{{$r->id}}" data-toggle="modal" id="modal-edit" title="Edit"><i class="fa fa-wrench"></i></a>

            </td>
            <td>
               
                   
                    <button class="btn btn-danger" type="submit" onclick="return confirm('ต้องการลบข้อมูลใช่ หรือ ไม่')"><i class="fa fa-trash"></i></button>
                
            </td>

        </tr>
        <div class="modal fade col-12" id="modal-edit-customers{{$r->id}}" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg  w3-animate-right">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="">รายละเอียด</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <input type="hidden" name="_token">
                    
                        <!-- Modal body -->
                        <div class="modal-body">

                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">ชื่อประเภทโปรเจ็ค</label>
                                    @foreach($officer_examsxx as $rows)
                                    <input type="text" class="form-control" name="detail" value="{{$rows->detail}}" placeholder="เวลา">
                                    <input class="form-control" name="date" type="date" value="{{$rows->date}}" id="example-date-input">
                                    <input type="hidden" name="exam_id" value="{{$r->id}}">
                                    @endforeach
                                    <p><i class="fas fa-align-center" style="font-size: 18px;  color:rgb(120,132,232);"></i> : โปรเจคที่คุมสอบ</p>
                                    <select class="custom " id="inputGroupSelect01" name="project_id">
                                        @foreach ($studentadexams as $ro)
                                        <option selected value="{{$ro->project_id}}">{{$ro->project_nameth}}</option>
                                        @endforeach
                                    </select><br><br>

                                    <hr>
                                    <p><i class="fas fa-users" style="font-size: 18px;  color:rgb(120,132,232);"></i> : อาจารย์คุมสอบ</p>
                                    <select name="officer_id[]" class="form-control selectpicker" multiple data-live-search="true" required="">
                                        @foreach ($officers as $s)
                                        <option value=""></option>
                                        <option value="{{ $s->id }}">{{ $s->officer_name }} {{ $s->officer_lastname }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="w3-button w3-green w3-round-xlarge">แก้ไข</button>
                            <button type="button" class="w3-button w3-red w3-round-xlarge" data-dismiss="modal">ออก</button>
                        </div>
                </div>
            </div>
          
            @endforeach
    </table>
</div>


@endsection