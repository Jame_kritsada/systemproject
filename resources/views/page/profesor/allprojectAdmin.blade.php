@extends('Layout.master')
@section('title')
โปรเจคทั้งหมด
@endsection('title')

@section('content')
<div class="container">
    <h2>โปรเจคทั้งหมด</h2>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>ลำดับ</th>
                <th>ชื่อโปรเจค</th>
                <th>รายละเอียด</th>
                <th>อนุมัติProject2</th>
            </tr>
        </thead>
        @foreach($project as $row)
        <tbody>
            <tr>

                <td>{{$row->project_nameth}}</td>
                <td>{{$row->pro1_pro2_status}}</td>
                <td>
                    <button type="button" style='font-size:24px' class="w3-button w3-blue w3-round-xlarge far fa-address-card" data-toggle="modal" data-target="#d1">
                    </button>
                </td>
                <td>
                    <button type="button" style='font-size:24px' class="w3-button w3-red w3-round-xlarge fas fa-dragon" data-toggle="modal" data-target="#d2">
                    </button>
                </td>

                <div class="modal fade" id="d1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-xl">

                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="" required="">รายละเอียดโปรเจ็ค</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="col-md-12 ">
                                    <i class="fas fa-align-center"></i>
                                        {{$row->project_detailth}}<hr><i class="fas fa-align-center"></i>
                                        {{$row->project_detailen}}

                                    </div>
                                </div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="modal fade" id="d2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-xl">

                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="" required="">รายละเอียดโปรเจ็ค</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="col-md-12 ">
                                        <form action="{{ url('upstatus_pro2',$row->project_id) }}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="pro1_pro2_status" value="Project2">
                                            <h1>ต้องการอนุมัติขึ้นProject2 หรือไม่</h1>

                                    </div>
                                </div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">YES</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                            </form>
                        </div>
                    </div>

                </div>
                @endforeach
        </tbody>

    </table>
    <!-- The Modal ***************************************************************************************************************************-->

</div>
@endsection

@section('footerscript')
@if(session('feedback'))

<script src="{{ asset('alert.js')}}"></script>

<script>
    swal("{{ session('feedback') }}", "ผลการทำงาน", "success");
</script>

@endif

@endsection