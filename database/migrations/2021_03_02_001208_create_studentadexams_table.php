<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentadexamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studentadexams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status_exam_adviser')->nullable();
            $table->string('filefirst')->nullable();
            $table->string('fileend')->nullable();
            $table->string('status_exam_admin')->nullable();
            $table->string('status_exam')->nullable();
            $table->integer('project_id')->unsigned();
            $table->integer('exam_id')->unsigned();
            $table->timestamps();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('exam_id')->references('id')->on('exams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studentadexams');
    }
}
