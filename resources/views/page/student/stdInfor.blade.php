@extends('Layout.masterstd')
@section('title')
@endsection('title')
@section('content')
<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
    <h4>&nbsp;&nbsp;ข้อมูลส่วนตัว</h4>
</div>
<div class="container">
@foreach($students as $row)
    <div class="row">
        &nbsp;&nbsp;&nbsp;&nbsp;<h4>
            <p class="font-weight-bold">ชื่อ :</p>
        </h4>&nbsp;<h4>{{$row->student_name}}&nbsp;&nbsp;{{$row->student_lastname}}</h4>
    </div>
    <div class="row mt-0">
        &nbsp;&nbsp;&nbsp;&nbsp;<h4>
            <p class="font-weight-bold">รหัสนิสิต :</p>
        </h4>&nbsp;<h4>{{$row->student_id}}</h4>
    </div>
    <div class="row mt-0">
        &nbsp;&nbsp;&nbsp;&nbsp;<h4>
            <p class="font-weight-bold">เบอร์โทรศัพท์ :</p>
        </h4>&nbsp;<h4>{{$row->tel_std}}</h4>
    </div>
    <a href="{{ action('StdinformationController@edit', $row->id) }}" class="btn btn-warning"><i class="fa fa-wrench"></i></a>
    @endforeach
</div>

@endsection