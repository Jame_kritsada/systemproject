@extends('Layout.master')
@section('title')
แก้ไขข้อมูลการสอบ
@endsection('title')

@section('content')

<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
    <h3>&nbsp;&nbsp;แก้ไขข้อมูลการสอบ</h3>
</div>
<div class="container">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    @foreach ($editexam as $row)


    <form action="{{ action('ExamController@update', $row->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <div class="form-row">
            <div class="form-group col-md-5">
                <label for="topic">เลือกสอบ</label>
                <select class="form-control" name="topic">
                    <option selected>{{ $row->topic }}</option>
                    <option value="midterm">กลางภาค</option>
                    <option value="final">ปลายภาค</option>
                </select>
            </div>
            <div class="form-group col-md-5">
                <label for="typeproject">โปรเจ็ค</label>
                <select class="form-control" name="typeproject">
                    <option selected>{{ $row->typeproject }}</option>
                    <option value="Project1">Project1</option>
                    <option value="Project2">Project2</option>
                </select>
                <br>
                <br>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="yearedu">ปีการศึกษา</label>
                <div class="row">
                    <div class="form-group col-md-3">
                        <input type="text" class="form-control" name="yearedu" value='{{ $row->yearedu }}' placeholder="ตัวอย่าง 2/2562">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="dateexamstart">วันที่เริ่มสอบ</label>
                <input type="date" value="{{ $row->dateexamstart }}" class="form-control" name="dateexamstart" required="">
            </div>
            <div class="form-group col-md-4">
                <label for="dateexamend">ถึงวันที่</label>
                <input type="date" class="form-control" value="{{ $row->dateexamend }}" name="dateexamend" required="">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="dateregisstart">ระยะเวลาขอขึ้นสอบ</label>
                <input type="date" class="form-control" value="{{ $row->dateregisstart }}" name="dateregisstart" required="">
            </div>
            <div class="form-group col-md-4">
                <label for="dateregisend">ถึงวันที่</label>
                <input type="date" class="form-control" value="{{ $row->dateregisend }}" name="dateregisend" required="">
            </div>
        </div>
        <div class="form-group col-md-8">
            <div class="form-group">
                <label for="detail">รายละเอียดการสอบ</label>
                <textarea class="form-control" rows="5" id="comment" name="detail">{{ $row->detail }}</textarea>
            </div>
        </div>


        
        @endforeach
        <div class=" text-center">
            <button type="submit" class="btn btn-primary " onclick="return confirm('ต้องการเพิ่มข้อมูลใช่ หรือ ไม่')"><i class="bx bx-plus-circle">เพิ่มข้อมูล</i></button>
        </div>
        <input type="hidden" name="_method" value="PATCH">
    </form>
</div>



@endsection