@extends('Layout.master')
@section('title')
สอบ
@endsection('title')

@section('content')

<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>

</div>

<div class="table-responsive">
    <table class="table  tabel-bordered  table-striped">
        <tr>
            <th>การสอบ</th>
            <th>โปรเจ็ค</th>
            <th>วิชา</th>
            <th>คะแนน</th>
            <th>แก้ไข</th>
        </tr>
        @foreach ($exams as $row)
        <tr>
            <td>@if($row->topic == 'midterm')กลางภาค
                @elseif($row->topic == 'final')ปลายภาค
                @endif
            </td>
            <td>{{ $row->project_nameth }}</td>
            <td>{{ $row->typeproject }}</td>
            <td>{{ $row->poin }}</td>
            <td><a class="btn btn-warning" data-target="#modal-edit-customers{{$row->id}}" data-toggle="modal" id="modal-edit" title="Edit"><i class="fa fa-wrench"></i></a>
            </td>
        </tr>
        <div class="modal fade col-12" id="modal-edit-customers{{$row->id}}" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg  w3-animate-right">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="">รายละเอียด</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <input type="hidden" name="_token">

                    <!-- Modal body -->
                    <div class="modal-body">

                        <div class="form-row">
                            <form action="{{ url('Examadminuppoin', $row->exam_id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="form-group col-md-4">
                                    <input type="text" name="poin" value="{{ $row->poin }}">
                                </div>

                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="w3-button w3-green w3-round-xlarge">แก้ไข</button>
                        <button type="button" class="w3-button w3-red w3-round-xlarge" data-dismiss="modal">ออก</button>
                    </div>
                    </form>
                </div>
            </div>

            @endforeach
    </table>
</div>


@endsection