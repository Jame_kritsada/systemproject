@extends('Layout.master')
@section('title')
    แก้ไขข้อมูลอาจารย์
@endsection('title')

@section('content')


    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">แก้ไขข้อมูลอาจารย์</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @foreach ($officer as $officer)

                            <form action="{{ action('OfficerController@update', $officer->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">ชื่อ</label>
                                        <input type="text" class="form-control" name="officer_name"
                                            value="{{ $officer->officer_name }}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">สกุล</label>
                                        <input type="text" class="form-control" name="officer_lastname"
                                            value="{{ $officer->officer_lastname }}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">email</label>
                                        <input type="text" class="form-control" name="email" value="{{ $officer->email }}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">password</label>
                                        <input type="text" class="form-control" name="password" value="{{ $officer->password }}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">เบอร์</label>
                                        <input type="text" class="form-control" name="tel_off"
                                            value="{{ $officer->tel_off }}">
                                    </div>

                                </div>
                                <div class=" text-center">
                                    <button type="submit" class="btn btn-primary "
                                        onclick="return confirm('ต้องการแก้ไขข้อมูลใช่ หรือ ไม่')">เพิ่มข้อมูล</button>
                                </div>
                                <input type="hidden" name="_method" value="PATCH">
                            </form>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
