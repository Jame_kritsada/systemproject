<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Adviser;
use App\User;
use App\Officer;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;

class OfficerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['officer'] = DB::table('officers')
            ->join('Users', 'officers.user_id', 'Users.id')
            ->select(
                'Users.email',
                'Users.password',
                'Users.id',
                'officers.officer_name',
                'officers.officer_lastname',
                'officers.tel_off',
                'officers.user_id as id'
                // )->orderBy('officers.of_name', 'ASC')
            )->get();
        return view('page.admin.AddOfficer', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), []);
        if (!$validator->fails()) {

            $user = new User;
            $user->type = $request->input('type');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->save();
            $id = $user->id;

            $officer = new officer;
            $officer->officer_name = $request->input('officer_name');
            $officer->officer_lastname = $request->input('officer_lastname');
            $officer->tel_off = $request->input('tel_off');
            $officer->user_id = $id;
            $officer->save();

            return redirect()->route('officer.index')->with('success', 'บันทึกสำเร็จ');
        } else {
            return redirect()->route('officer.index')->with('warning', 'บันทึกไม่สำเร็จ');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['officer'] = DB::table('users')
            ->join('officers', 'users.id', 'officers.user_id')
            ->select(
                'Users.email',
                'Users.password',
                'Users.id',
                'officers.officer_name',
                'officers.officer_lastname',
                'officers.tel_off',
                'officers.id as of_id'
                // )->orderBy('officers.of_name', 'ASC')
            )
            ->where('users.id', $id)
            ->get();
        return view('page.admin.EditOfficer', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([]);

        $officer = Officer::where('user_id', $id)->first();
        $officer->officer_name = $request->get('officer_name');
        $officer->officer_lastname = $request->get('officer_lastname');
        $officer->tel_off = $request->get('tel_off');

        $officer->save();
        // dd($officer);

        $user = User::find($id);
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->save();

        // dd($user);
        return redirect()->route('officer.index')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        $ids = DB::table('officers')->select('id')
            ->where('user_id', '=', $user_id)->first();
        $officer = officer::find($ids->id);
        $officer->delete();
        $user = user::find($user_id);
        $user->delete();

        return redirect()->route('officer.index')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
