<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use App\Student;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class StdinformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idu = Auth::user()->id;

        $data['students'] = DB::table('students')
            ->join('Users', 'Students.user_id', 'Users.id')
            ->select(
                'Users.id',
                'Users.email',
                'students.student_name',
                'students.student_lastname',
                'students.student_id',
                'students.tel_std'
                // )->orderBy('officers.of_name', 'ASC')
            )->where('Users.id','=',$idu)->get();

        return view('page.student.stdinfor', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idu = Auth::user()->id;
        $data['students'] = DB::table('students')
            ->join('Users', 'Students.user_id', 'Users.id')
            ->select(
                'Users.id',
                'Users.email',
                'students.student_name',
                'students.student_lastname',
                'students.student_id',
                'students.tel_std'
                // )->orderBy('officers.of_name', 'ASC')
            )->where('Users.id','=',$idu)->get();

        return view('page.student.EditstdInfor', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
                  
            ]);
            $student = Student::find($id);
            $student ->student_name = $request->input('student_name');
            $student ->student_lastname = $request->input('student_lastname');
            $student ->student_id = $request->input('student_id');
            $student ->tel_std = $request->get('tel_std');
            $student->save();
            return redirect()->route('stdinfor.index')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
