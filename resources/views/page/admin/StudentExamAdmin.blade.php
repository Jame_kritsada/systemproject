@extends('Layout.master')
@section('title')
คำขอขึ้นสอบ
@endsection('title')

@section('content')

<div class="section-title">
    <h2>คำขอขึ้นสอบ</h2>
</div>
<table class="table table-striped">

    <tr>
        <th class="column3">ชื่อโปรเจ็ค</th>
        <th class="column3">สอบ</th>
        <th class="column3">วิชา</th>
        <th class="column3">รายละเอียดโปรเจค</th>
        <th class="column5">อนุมัติ</th>
        <th class="column6">ไม่อนุมัติ</th>
    </tr>

    <tbody>
        @foreach ($studentexamofficer as $row)

        <tr>
            <td>{{ $row->project_nameth }}</td>
            <td>{{ $row->topic }}</td>
            <td>{{ $row->pro1_pro2_status }}</td>

            <td>
                <a href="{{ url('detel_studentexamAD', $row->project_nameth) }}" type="submit" style='font-size:15px' class="w3-button w3-blue w3-round-xlarge far " method="get">แสดงรายละเอียด</a>
            </td>

            <td class="column5">
                <form action="{{ url('upstatus_studentexamAD', $row->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <button onclick="myFunction()" type="submit" style='font-size:15px' class="w3-button w3-green w3-round-xlarge far " name="status_exam_admin" value="ผ่านการอนุมัติ">อนุมัติ</button>
                    <script>
                        function myFunction() {
                            alert("You pressed OK!");
                        }
                    </script>
                </form>
            </td>

            <td class="column6">
                <button type="button" class="w3-button w3-red w3-round-xlarge far " data-toggle="modal" data-target="#1">
                    ไม่อนุมัติ
                </button>
            </td>

        </tr>



        <div class="modal fade" id="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl">

                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="" required="">ความคิดเห็น</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form action="{{ url('upstatus_studentexamAD', $row->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-12 ">
                                    <input type="hidden" name="status_exam_admin" value="ไม่ผ่านการอนุมัติ">
                                    <textarea name="detail" class="form-control" id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>



                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </tbody>
    @endforeach
</table>


<div class="section-title">
    <h2>สถานะขอสอบ</h2>
</div>

<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>ชื่อโปรเจ็ค</th>
                <th>สอบ</th>
                <th>วิชา</th>
                <th scope="col">สถานะอาจารย์ที่ปรึกษา</th>
                <th scope="col">สถานะอาจารย์ประจำวิชา</th>
                <th scope="col">รายละเอียด</th>

            </tr>
        </thead>
        <tbody>
            @foreach($projectss as $row)
            <tr>

                <td>{{ $row->project_nameth }}</td>
                <td>{{ $row->topic }}</td>
                <td>{{ $row->pro1_pro2_status }}</td>
                <td>{{$row->status_exam_adviser}}</td>
                <td>{{$row->status_exam_admin}}</td>
                <td class="column6">
                    <button type="button" class="bx bx-comment-detail  btn btn-primary " data-toggle="modal" data-target="#d2">
                    </button>
                </td>



            </tr>


            <div class="modal fade" id="d2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl">

                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4>รายละเอียดการสอบ</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-12 ">
                                <i class="fas fa-align-center"></i>
                                    ระดับโปรเจค -> {{$row->pro1_pro2_status}} <hr><i class="fas fa-align-center"></i>
                                    ชื่อโปรเจค -> {{$row->project_nameth}} <hr><i class="fas fa-align-center"></i>
                                    ชื่อโปรเจค -> {{$row->project_nameen}} <hr><i class="fas fa-align-center"></i>
                                    รายละเอียดโปรเจค -> {{$row->project_detailth}} <hr><i class="fas fa-align-center"></i>
                                    รายละเอียดโปรเจค -> {{$row->project_detailen}}<hr><i class="fas fa-align-center"></i>
                                    สถานะสอบกลางภาค -> {{$row->status_midterm}}<hr><i class="fas fa-align-center"></i>
                                    สถานะสอบปลายภาค -> {{$row->status_final}} <br>
                                </div>
                            </div>

                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>

            </div>


        </tbody>
        @endforeach

    </table>
</div>

@endsection