<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Project;
use App\membersproject;
use App\Adviser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class myProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idu = Auth::user()->id;
        $ids = DB::table('students')->select('id')
            ->where('user_id', '=', $idu)->first();

        // dd($ids);
        // $officers = officer::find($ids->id);
        // $officers->delete();



        $data['membersprojects'] = DB::table('membersprojects')
            ->join('projects', 'membersprojects.projects_id', 'projects.id')
            ->select(
                'projects.id',
                'projects.project_nameth',
                'projects.project_nameen',
                'projects.status_topic_adviser',
                'projects.status_topic_admin',
                'projects.pro1_pro2_status',
                'projects.created_at'
            )->where('membersprojects.students_id', '=', $ids->id)->get();

        $data['exams'] = DB::table('exams')
            ->select(
                'id',
                'topic',
                'typeproject',
            )->get();

        // dd($data['membersprojects']);
        return view('page.student.myProject', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        // $idu = Auth::user()->id;
        // $data['project'] = DB::tabl('membersprojects')
        // ->where('students_id',$idu)
        // ->get();
        // dd($data);
        // return view('page.student.upFileFull',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        //dd($id);
        if ($request->file('file_name') != '') {

            $file = $request->file('file_name');
            $ext = $file->getClientOriginalExtension();

            $name = md5(rand() * time()) . '.' . $ext;
            $file->move(public_path('file_full'), $name);
        } else {
            $name = '';
        }

        //dd($name);
        $studentfile = Project::find($id);
        //dd($studentfile);
        $studentfile->file_full =  $name;
        //dd($studentfile);
        $studentfile->save();
        return redirect(url('home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['students'] = DB::table('students')
            ->select(
                'students.id',
                'students.student_name',
                'students.student_lastname',
                'students.student_id',
                'students.tel_std'
            )->orderBy('students.student_name', 'ASC')
            ->get();
            $data['mystudents'] = DB::table('students')
            ->select(
                'students.id',
                'students.student_name',
                'students.student_lastname',
                'students.student_id',
                'students.tel_std'
            )->where('id','=',$id)
            ->get();

        $data['project_types'] = DB::table('project_types')
            ->get();
        $data['myproject_types'] = DB::table('project_types')
            ->where('id', '=', $id)
            ->get();
        //dd($data['project_types']);
        $data['officer'] = DB::table('officers')
        ->where('id','=',$id)
            ->get();

        $data['Editmembersprojects'] = DB::table('membersprojects')
            ->join('projects', 'membersprojects.projects_id', 'projects.id')
            ->select(
                'projects.id',
                'projects.project_nameth',
                'projects.project_nameen',
                'projects.project_detailth',
                'projects.project_detailen',
                'projects.status_topic_adviser',
                'projects.status_topic_admin',
                'projects.pro1_pro2_status',
                'projects.created_at'
            )->where('membersprojects.students_id', '=', $id)->get();

        $data['exams'] = DB::table('exams')
            ->select(
                'id',
                'topic',
                'typeproject',
            )->get();

        // dd($data['membersprojects']);
        return view('page.student.editstatusproject', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $Projects = Project::find($id);
        $Projects->project_nameth = $request->input('project_nameth');
        $Projects->project_nameen = $request->input('project_nameen');
        $Projects->project_detailth = $request->input('project_detailth');
        $Projects->project_detailen = $request->input('project_detailen');
        $Projects->status_topic_adviser = $request->input('status_topic_adviser');
        $Projects->status_topic_admin = $request->input('status_topic_admin');

        $Projects->save();
        $id = $Projects->id;

        $student = $request->input('students_id');
        $size = count(collect($student));

        for ($i = 0; $i < $size; $i++) {
            $student1 = new membersproject();
            $student1->projects_id = $id;
            $student1->students_id =  $student[$i];
            $student1->save();
        }

        $officer_id = $request->input('officer_id');
        $size = count(collect($officer_id));

        for ($i = 0; $i < $size; $i++) {
            $Adviser = new Adviser();
            $Adviser->projects_id = $id;
            $Adviser->officer_id = $officer_id[$i];
            $Adviser->save();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $project = Project::find($id);
        //dd($project);
        $project->delete();
        return redirect()->route('myProject.index')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
