@extends('Layout.master')
@section('title')
    เพิ่มข้อมูลอาจารย์
@endsection('title')

@section('content')

    <div class="container">
        <div class="table-responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">เพิ่มข้อมูล อาจารย์</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ url('createAdviser') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-row">

                                <input type="hidden" name="type" value="officer">

                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">username</label>
                                    <input type="text" class="form-control" name="name" required="">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">email</label>
                                    <input type="email" class="form-control" name="email" required="">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">password</label>
                                    <input type="password" class="form-control" name="password" required="">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">รหัสอาจารย์</label>
                                    <input type="text" class="form-control" name="adviser_id" 
                                        title="กรอกตัวเลขเท่านั้น" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">ชื่อ</label>
                                    <input type="text" class="form-control" name="adviser_name" pattern=^[ก-๏\s]+$
                                        title="กรอกตัวอักษรเท่านั้น" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">สกุล</label>
                                    <input type="text" class="form-control" name="adviser_lastname" pattern=^[ก-๏\s]+$
                                        title="กรอกตัวอักษรเท่านั้น" required>
                                </div>
                             
                                <input type="hidden" name="off_id" value="1">
                                
                            </div>
                            <div class=" text-center">
                                <button type="submit" class="btn btn-primary "
                                    onclick="return confirm('ต้องการเพิ่มข้อมูลใช่ หรือ ไม่')">เพิ่มข้อมูล</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="table-responsive">
            <table class="table  tabel-bordered  table-striped">
                <tr>
                    
                    <th>รหัสอาจารย์</th>
                    <th>ชื่อ</th>
                    <th>สกุล</th>
                    <th>แก้ไข</th>
                    <th>ลบ</th>
                </tr>
                @foreach ($adviser as $row)
                    <tr>
                        
                        <td>{{ $row->adviser_id }}</td>
                        <td>{{ $row->adviser_name }}</td>
                        <td>{{ $row->adviser_lastname }}</td>
                        
                        <td>
                            <a href="{{ action('AdviserController@edit', $row->id) }}" class="btn btn-warning"><i
                                    class="fa fa-wrench"></i></a>
                        </td>
                        <td>
                            <form method="post" class="delete_form"
                                action="{{ action('AdviserController@destroy', $row->id) }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger" type="submit"
                                    onclick="return confirm('ต้องการลบข้อมูลใช่ หรือ ไม่')"><i
                                        class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection