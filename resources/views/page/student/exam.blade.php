@extends('Layout.masterstd')
@section('title')
@endsection('title')
@section('content')
<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
</div>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" href="{{ url('exam/show')}}" role="tab" aria-controls="pills-home" aria-selected="true">รายการสอบ</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ url('exam/showstatus')}}" role="tab" aria-controls="pills-profile" aria-selected="false">สถานะสอบ</a>
    </li>

</ul>
<table class="table">
    <thead>
        <tr>
            <th scope="col">ชื่อ</th>
            <th scope="col">วิชา</th>
            <th scope="col">รายละเอียด</th>
            <th scope="col">ยื่นขอสอบ</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($exams as $row)
        <tr>
            @if( $row->topic == 'midterm')
            <td>กลางภาค</td>
            @elseif( $row->topic == 'final')
            <td>ปลายภาค</td>
            @endif
            <td>{{$row->typeproject}}</td>
            <td class="column6">
                <button type="button" class="bx bx-comment-detail  btn btn-primary " data-toggle="modal" data-target="#d1">
                </button>
            </td>

            <td>
                <form action="{{ url('Requestexam',$row->id,) }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" name="id" value="{{ $row->id }}">

                    <button type="submit" class=" btn btn-danger "> ยื่นขอสอบ</button>
                </form>
            </td>

        </tr>




        <div class="modal fade" id="d1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl">

                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4>รายละเอียดการสอบ</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 ">
                                <!-- {{$row->id}} -->
                                <i class="fas fa-align-center"></i>
                                การสอบ {{$row->topic}}
                                <hr><i class="fas fa-align-center"></i>
                                ประเภท {{$row->typeproject}}
                                <hr><i class="fas fa-align-center"></i>
                                วันที่ {{$row->dateregisstart}} - {{$row->dateregisend}}
                            </div>
                        </div>

                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>


    </tbody>
    @endforeach

</table>

<style>
    .blink {
        animation: blinker 0.6s linear infinite;
        color: #1c87c9;
        font-size: 30px;
        font-weight: bold;
        font-family: sans-serif;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

    .blink-one {
        animation: blinker-one 1s linear infinite;
    }

    @keyframes blinker-one {
        0% {
            opacity: 0;
        }
    }

    .blink-two {
        animation: blinker-two 1.4s linear infinite;
    }

    @keyframes blinker-two {
        100% {
            opacity: 0;
        }
    }
</style>
@endsection