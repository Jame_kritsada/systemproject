@extends('Layout.master')
@section('title')
เพิ่มข้อมูลอาจารย์
@endsection('title')

@section('content')
    <div class="table-responsive">
        <div class="col-md-12">
                <div class="card-header">เพิ่มข้อมูล อาจารย์</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form action="{{ route('officer.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <input type="hidden" name="type" value="officer">
                            <div class="form-group col-md-4">
                                <label for="inputPassword4">email</label>
                                <input type="email" class="form-control" name="email" required="">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputPassword4">password</label>
                                <input type="password" class="form-control" name="password" required="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="inputPassword4">ชื่อ</label>
                                <input type="text" class="form-control" name="officer_name" pattern="^[ก-๏\s]+$" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputPassword4">สกุล</label>
                                <input type="text" class="form-control" name="officer_lastname" pattern="^[ก-๏\s]+$" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="inputPassword4">เบอร์</label>
                                <input type="text" class="form-control" name="tel_off" pattern="^[0-9\s]+$" minlength="0" maxlength="12" required>
                            </div>
                        </div>
                </div>
            <div class=" text-center">
                <button type="submit" class="btn btn-primary " onclick="return confirm('ต้องการเพิ่มข้อมูลใช่ หรือ ไม่')">เพิ่มข้อมูล</button>
            </div>
            </form>
        </div>
    </div>
<br>
    <div class="table-responsive">
        <table class="table  tabel-bordered  table-striped">
            <thead>
                <tr>
                    <th>ชื่อ</th>
                    <th>สกุล</th>
                    <th>E-mail</th>
                    <th>เบอร์</th>
                    <th>แก้ไข</th>
                    <th>ลบ</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($officer as $row)
            <tr>

                <td>{{ $row->officer_name }}</td>
                <td>{{ $row->officer_lastname }}</td>
                <td>{{ $row->email }}</td>
                <td>{{ $row->tel_off }}</td>

                <td>
                    <a href="{{ action('OfficerController@edit', $row->id) }}" class="btn btn-warning"><i class="fa fa-wrench"></i></a>
                </td>
                <td>
                    <form method="post" class="delete_form" action="{{ action('OfficerController@destroy', $row->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="submit" onclick="return confirm('ต้องการลบข้อมูลใช่ หรือ ไม่')"><i class="fa fa-trash"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection