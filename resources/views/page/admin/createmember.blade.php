@extends('Layout.master')
@section('title')
    จัดการสมาชิก
@endsection('title')

@section('content')
    <div class="container ">
        <h2>เพิ่มข้อมูลรถ</h2>
    </div>
    <form id="formInsertPost" action="{{ url('/post') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-row">
            <div class="col-md-5 mb-3">
                <label for="validationDefault01"> ชื่อรุ่น</label>
                <input type="text" class="form-control" name="name" required>
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationDefault02">ทะเบียน</label>
                <input type="email" class="form-control" name="email" required>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-3 mb-3">
                <label for="validationDefault04">ยี่ห้อ</label>
                <input type="text" class="form-control" name="c_brand" required>
            </div>

            <div class="col-md-3 mb-3">
                <label for="validationDefault04">ประเภท</label>
                <select name="TyCar_id" class="custom-select mb-3">
                    <option>------------ เลือก ----------</option>
                    @foreach ($posts as $post)
                        <option value="{{ $post->TypeCar_id }}">{{ $post->name_typCar }}
                        </option>
                    @endforeach

                </select>
            </div>
            <div class="col-md-3 mb-3">
                <label for="validationDefault03">สถานะ</label>
                <select name="c_status" class="custom-select mb-3">
                    <option>------------ เลือก ----------</option>
                    <option value="พร้อมใช้งาน">พร้อมใช้งาน</option>
                    <option value="ไม่พร้อมใช้งาน">ไม่พร้อมใช้งาน</option>

                </select>
            </div>
        </div>
        <div class="col-md-3 mb-3 ">
            <input type="file" class="custom-file-input" id="customFile" name="image" required>
            <label class="custom-file-label" for="customFile">รูปภาพ</label>
        </div>
        <div class="form-group">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required>
                <label class="form-check-label" for="invalidCheck2">
                    Agree to terms and conditions
                </label>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">เพิ่มข้อมูล</button>





    </form>


@endsection
@section('footerscript')
    @if (session('feedback'))

        <script src="{{ asset('alert.js') }}"></script>

        <script>
            swal("{{ session('feedback') }}", "ผลการทำงาน", "success");

        </script>

    @endif

@endsection
