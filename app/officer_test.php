<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class officer_test extends Model
{
    protected $fillable = [
        'score_officer1',
        'score_officer2',
        'score_officer3',
        'status_officertest',
        'reqexam_id',
        ];
      protected $primaryKey = 'id';
}
