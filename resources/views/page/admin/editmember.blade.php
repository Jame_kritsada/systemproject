@extends('Layout.master')
@section('title')
แก้ไขการสมาชิก
@endsection('title')

@section('content')


<div class="card-header">เพิ่มข้อมูล อาจารย์</div>
<div class="card-body">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif

    @foreach($editmember as $row)
    <form action="{{ action('Member1Controller@update', $row->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
       
        <div class="form-row">
            <div class="col-md-3 mb-4">
                <label for="validationDefault03">E-mail</label>
                <input type="text" class="form-control" name="email" value="{{$row->email}}">
            </div>
            <div class="col-md-2 mb-4">
                <label for="validationDefault04">Password</label>
                <input type="text" class="form-control" name="password" value="{{$row->password}}">
            </div>

        </div>
        <div class=" text-center">
            <button type="submit" class="btn btn-primary " onclick="return confirm('ต้องการแก้ไขข้อมูลใช่ หรือ ไม่')">เพิ่มข้อมูล</button>
        </div>
        <input type="hidden" name="_method" value="PATCH">
    </form>
</div>

@endforeach
@endsection