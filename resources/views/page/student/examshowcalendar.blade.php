@extends('Layout.masterstd')
@section('title')
@endsection('title')
@section('content')
<div class="section-title">
  <h2>INFORMATION TECHNOLOGY</h2>
</div>

@foreach ($exam as $row)
<div class="card">


  <div  style="margin-left: 10px;" class="card-body">
    <h4 style="margin-left: 300px;">สอบ@if($row->topic == 'midterm')กลางภาค {{$row->typeproject}}@elseif($row->topic == 'final')ปลายภาค {{$row->typeproject}}@endif ปีการศึกษา{{$row->yearedu}}
    </h4><hr>
    <div   style="margin-left: 90px;" >
    <h5 class="card-title"><i class="fas fa-font"></i> ชื่อโปรเจค : {{$row->project_nameth}}</h5>
    <?php
                                                                            $con = mysqli_connect("127.0.0.1", "root", "", "systemproject");
                                                                            // Check connection
                                                                            if (mysqli_connect_errno()) {
                                                                              echo "Failed to connect to MySQL: " . mysqli_connect_error();
                                                                            }
                                                                            $id = $row->id;
                                                                            $result = mysqli_query($con, "SELECT * FROM officer_examsview WHERE studentadexams_id = $id GROUP BY detail ");



                                                                            while ($ro = mysqli_fetch_array($result)) {

                                                                            ?>

        <h5 class="card-title"><i class="fas fa-align-center"></i> วันที่/เวลาสอบ 
          <?php echo  $ro['date'] .  $ro['detail']; ?>


      <?php  }


      ?>
    </h5>
    <p class="card-text">
    <p>อาจารย์คุมสอบ<?php 
                    $con = mysqli_connect("127.0.0.1", "root", "", "systemproject");
                    // Check connection
                    if (mysqli_connect_errno()) {
                      echo "Failed to connect to MySQL: " . mysqli_connect_error();
                    }
                    $id = $row->id;
                    $result = mysqli_query($con, "SELECT * FROM officer_examsview WHERE studentadexams_id = $id ");



                    while ($ro = mysqli_fetch_array($result)) {

                    ?>

    <div class="mb-3"><i class="fas fa-users"></i>
      &nbsp;&nbsp;&nbsp;&nbsp;<?php echo  $ro['officer_name']; ?> <?php echo  $ro['officer_lastname']; ?>
      <!-- <?php echo  $ro['detail']; ?>
      <?php echo  $ro['date']; ?> -->
    </div>

  <?php  }


  ?></p>

  @if($row->status_exam == 'ผ่าน')
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#d1">
    ส่งไฟล์หลังสอบ
  </button>
  @endif
  </div>
</div>
<div class="modal fade" id="d1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-xl">

    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="" required="">ความคิดเห็น</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="{{ url('studentFileEnd', $row->id) }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}

        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-row">
            <div class="col-md-12 ">

              <input type="file" name="file_name" class="custom-file-input" id="inputGroupFile01">
              <label class="custom-file-label" for="inputGroupFile01">แนบไฟล์</label>
            </div>
          </div>



        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
  </div>
</div>
@endforeach
@endsection