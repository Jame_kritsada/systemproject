<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adviser extends Model
{
  protected $fillable = [
    'projects_id',
    'officer_groupprojecttypes_id',
  ];
  protected $primaryKey = 'id';
}
