@extends('Layout.master')
@section('title')
คำขอขึ้นสอบ
@endsection('title')

@section('content')

<div class="section-title">
    <h2>คำขอขึ้นสอบ</h2>
</div>
<table class="table table-hover">
    <thead>
        <tr>
            <th class="column3">ชื่อโปรเจ็ค</th>
            <th class="column3">ปีการศึกษา</th>
            <th class="column3">รายละเอียดโปรเจ็ค</th>
            <th class="column5">อนุมัติ</th>
            <th class="column6">ไม่อนุมัติ</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($studentexamofficer as $row)
        <tr>
            <td>{{ $row->project_nameth }}</td>
            <td>{{ $row->yearedu }}</td>
            <td>
                <a href="{{ url('detel_studentexam', $row->project_nameth) }}" type="submit" style='font-size:15px' class="w3-button w3-blue w3-round-xlarge far " method="get">แสดงรายละเอียด</a>
            </td>

            <td class="column5">
                <form action="{{ url('upstatus_studentexam', $row->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <button onclick="myFunction()" type="submit" style='font-size:15px' class="w3-button w3-green w3-round-xlarge far " name="status_exam_adviser" value="ผ่านการอนุมัติ">อนุมัติ</button>
                    <script>
                        function myFunction() {
                            alert("You pressed OK!");
                        }
                    </script>
                </form>
            </td>

            <td class="column6">
                <button type="button" class="w3-button w3-red w3-round-xlarge far " data-toggle="modal" data-target="#d1">
                    ไม่อนุมัติ
                </button>
            </td>

        </tr>



        <div class="modal fade" id="d1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl">

                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="" required="">ความคิดเห็น</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form action="{{ url('upstatus_studentexam', $row->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-12 ">
                                    <input type="hidden" name="status_exam_adviser" value="ไม่ผ่านการอนุมัติ">
                                    <textarea name="detail" class="form-control" id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>



                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </tbody>
    @endforeach
</table>

@endsection