@extends('Layout.master')
@section('title')
    รายละเอียดคำขอเป็นที่ปรึกษา
@endsection('title')

@section('content')

    <div class="section-title">
        <h2>รายละเอียดคำขอเป็นที่ปรึกษา</h2>
        @foreach ($projectss as $row)
            <h2>&nbsp;&nbsp;ชื่อโปรเจ็คภาษาไทย {{ $row->project_nameth }} <br>ชื่อโปรเจ็คภาษาอังกฤษ {{ $row->project_nameen }}</h2>
    </div>
    <div class="container">
        <div class="row">
            &nbsp;&nbsp;&nbsp;&nbsp;<h4>
                <p class="font-weight-bold">Pro1:Pro2 :</p>
            </h4>&nbsp;<h4>{{ $row->pro1_pro2_status }}</h4>
        </div>
        <div class="row mt-0">
            &nbsp;&nbsp;&nbsp;&nbsp;<h4>
                <p class="font-weight-bold">รายละเอียด :</p>
            </h4>&nbsp;<h4>{{ $row->project_detailth }}</h4>
        </div>
        <div class="row mt-0">
            &nbsp;&nbsp;&nbsp;&nbsp;<h4>
                <p class="font-weight-bold">รายละเอียด :</p>
            </h4>&nbsp;<h4>{{ $row->project_detailen }}</h4>
        </div>
        <div class="row mt-0">
            &nbsp;&nbsp;&nbsp;&nbsp;<h4>
                <p class="font-weight-bold">file_project :</p>
            </h4>&nbsp;<h4>{{ $row->file_project }}</h4>
        </div>
        <div class="row mt-0">
            &nbsp;&nbsp;&nbsp;&nbsp;<h4>
                <p class="font-weight-bold">file_full :</p>
            </h4>&nbsp;<h4>{{ $row->file_full }}</h4>
        </div>
        @endforeach
        <hr>
        @foreach ($student as $row)

            <div class="row mt-0">
                &nbsp;&nbsp;&nbsp;&nbsp;<h4>
                    <p class="font-weight-bold">รหัสนิสิต :</p>
                </h4>&nbsp;<h4>{{ $row->student_id }}</h4>
            </div>
            <div class="row mt-0">
                &nbsp;&nbsp;&nbsp;&nbsp;<h4>
                    <p class="font-weight-bold">ชื่อ-สกุล :</p>
                </h4>&nbsp;<h4>{{ $row->student_name }}&nbsp;&nbsp;{{ $row->student_lastname }}</h4>
            </div>
            <hr>


        @endforeach
    </div>

@endsection
