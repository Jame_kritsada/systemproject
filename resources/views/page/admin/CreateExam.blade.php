@extends('Layout.master')
@section('title')
@endsection('title')
@section('content')


<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
    <div class="row">
        <!-- &nbsp;&nbsp; <a class="btn btn-info active">สร้างประกาศสอบ</a>&nbsp;&nbsp; -->
        <!-- <a class="btn btn-info" href="exam/manage">จัดการการสอบ</a> -->
    </div>
</div>
<div class="container">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    <form action="{{ route('createExam.store') }}" method="post">
        {{ csrf_field() }}
        <div class="form-row">
            <input type="hidden" name="status" id="" value="ใช้งาน">
            <div class="form-group col-md-5">
                <label for="topic">เลือกสอบ</label>
                <select class="form-control" name="topic" required="">
                    <option selected>เลือก...</option>
                    <option value="midterm">กลางภาค</option>
                    <option value="final">ปลายภาค</option>
                </select>
            </div>
            <div class="form-group col-md-5">
                <label for="typeproject">โปรเจค</label>
                <select class="form-control" name="typeproject" required="">
                    <option selected>เลือก...</option>
                    <option value="Project1">Project1</option>
                    <option value="Project2">Project2</option>
                </select>
                <br>
                <br>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="yearedu">ปีการศึกษา</label>
                <div class="row">
                    <div class="form-group col-md-3">
                        <input type="text" class="form-control" name="yearedu" placeholder="ตัวอย่าง 2/2562" required="">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="dateexamstart">วันที่เริ่มสอบ</label>
                <input type="date" class="form-control" min="<?php echo date('Y-m-d'); ?>" name="dateexamstart" required="">
            </div>
            <div class="form-group col-md-4">
                <label for="dateexamend">ถึงวันที่</label>
                <input type="date" class="form-control" min="<?php echo date('Y-m-d'); ?>" name="dateexamend" required="">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="dateregisstart">วันขอขึ้นสอบ</label>
                <input type="date" class="form-control" min="<?php echo date('Y-m-d'); ?>" name="dateregisstart" required="">
            </div>
            <div class="form-group col-md-4">
                <label for="dateregisend">ถึงวันที่</label>
                <input type="date" class="form-control" min="<?php echo date('Y-m-d'); ?>" name="dateregisend" required="">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-8">
                <div class="form-group">
                    <label for="detail">รายละเอียดการสอบ</label>
                    <textarea class="form-control" rows="5" id="comment" name="detail" required=""></textarea>
                </div>
            </div>
        </div>

        <div class=" text-center">
            <button type="submit" class="btn btn-primary " onclick="return confirm('ต้องการเพิ่มข้อมูลใช่ หรือ ไม่')"><i class="bx bx-plus-circle">เพิ่มข้อมูล</i></button>
        </div>
    </form>
</div>

<hr>


<div class="table-responsive">
    <table class="table  tabel-bordered  table-striped">
        <tr>
            <th>การสอบ</th>
            <th>โปรเจ็ค</th>
            <th>ปีการศึกษา</th>
            <th>วันที่เริ่มสอบ</th>
            <th>จัดการสอบ</th>
            <th>Reportคะแนน</th>
            <th>ตารางเวลาสอบ</th>
            <th>แก้ไข</th>
            <th>ลบ</th>
        </tr>
        @foreach ($addexam as $row)
        <tr>
            <td>@if($row->topic == 'midterm')กลางภาค
                @elseif($row->topic == 'final')ปลายภาค
                @endif
            </td>
            <td>{{ $row->typeproject }}</td>
            <td>{{ $row->yearedu }}</td>
            <td>{{ $row->dateexamstart }} - {{ $row->dateexamend }}</td>
            <td> <a class="btn btn-info" href="{{url('exam/manage', $row->id)}}"><i class="fas fa-tasks"></i></a></td>
            <td>
                <a href="{{ url('examsadmin/report', $row->id) }}" class="btn btn-info"><i class="fas fa-print"></i></a>
            </td>
            <td>
                <a href="{{ url('showcalandarexam', $row->id) }}" class="btn btn-warning"><i class="fa fa-print"></i></a>
            </td>
            <td>
                <a href="{{ action('ExamController@edit', $row->id) }}" class="btn btn-warning"><i class="fa fa-wrench"></i></a>
            </td>
            <td>
                <form method="post" class="delete_form" action="{{ action('ExamController@destroy', $row->id) }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <button class="btn btn-danger" type="submit" onclick="return confirm('ต้องการลบข้อมูลใช่ หรือ ไม่')"><i class="fa fa-trash"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>




@endsection