@extends('Layout.master')
@section('title')
    ขอขึ้นสอบ
@endsection('title')

@section('content')
    <div class="container">
        <div class="section-title">
            <h2>โปรเจคที่ขอขึ้นสอบ</h2>
        </div>
        <table class="table table-hover">
            <thead>
                <tr>


                    <th>ชื่อโปรเจค</th>
                    <th>รายละเอียด</th>
                    <th>อนุมัติขึ้นสอบ</th>
                    <th>ไม่อนุมัติ</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($project as $row)

                    <tr>

                        {{-- <td>{{ $row->pro1_pro2_status }}</td> --}}
                        <td>{{ $row->project_name }}</td>
                        <td>
                            <a href="{{ url('detel', $row->project_name) }}" type="submit" style='font-size:15px'
                                class="w3-button w3-blue w3-round-xlarge far " method="get">แสดงรายละเอียด</a>
                        </td>
                        {{-- <td>{{ $row->student_id }} {{ $row->student_name }} {{ $row->student_lastname }} <br></td> --}}
                        {{-- <td>{{ $row->project_detail }}</td> --}}
                        {{-- <td>{{ $row->file_project }}</td>
                            <td>{{ $row->file_full }}</td>
                            <td>{{ $row->status_finished_notfinished }}</td>
                            <td>{{ $row->status_topic_adviser }}</td>
                            <td>{{ $row->status_topic_admin }}</td>
                            <td>{{ $row->status_midterm }}</td>
                            <td>{{ $row->status_final }}</td> --}}

                        <td class="column5">
                            <form action="{{ url('upstatus2', $row->project_name) }}" method="post"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <button onclick="myFunction()" type="submit" style='font-size:15px'
                                    class="w3-button w3-green w3-round-xlarge far " name="status_midterm"
                                    value="อนุมัติขึ้นสอบmidtermAdmin">อนุมัติ</button>
                                <script>
                                    function myFunction() {
                                        alert("You pressed OK!");
                                    }

                                </script>
                            </form>
                        </td>

                        <td class="column6">
                            <form action="{{ url('upstatus2', $row->project_name) }}" method="post"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <button onclick="myFunction2()" type="submit" style='font-size:15px'
                                    class="w3-button w3-red w3-round-xlarge far " name="status_midterm"
                                    value="ไม่อนุมัติขึ้นสอบmidtermAdmin">ไม่อนุมัติ</button>
                                <script>
                                    function myFunction2() {
                                        alert("You pressed Cancel!");
                                    }

                                </script>
                            </form>
                        </td>


                    </tr>

                @endforeach

            </tbody>

        </table>

    </div>
@endsection
