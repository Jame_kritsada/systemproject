<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
  protected $fillable = [
    
    'student_id',
    'student_name',
    'student_lastname',
    'tel_std',
    'user_id',
  ];
  protected $primaryKey = 'id';
  protected $table = 'Students';
}
