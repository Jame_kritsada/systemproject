<?php

namespace App\Http\Controllers;

use App\Adviser;
use App\membersproject;
use App\Project;
use App\Project_type;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class createProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['students'] = DB::table('students')
            ->select(
                'students.id',
                'students.student_name',
                'students.student_lastname',
                'students.student_id',
                'students.tel_std'
            )->orderBy('students.student_name', 'ASC')
            ->get();

        $data['project_types'] = DB::table('project_types')
            ->get();
        $data['officer'] = DB::table('officers')
            ->get();
        return view('page.student.createProject', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Project = new Project();
        $Project->pro1_pro2_status = $request->input('pro1_pro2_status');
        $Project->project_nameth = $request->input('project_nameth');
        $Project->project_nameen = $request->input('project_nameen');
        $Project->project_detailth = $request->input('project_detailth');
        $Project->project_detailen = $request->input('project_detailen');
        $Project->status_topic_adviser = $request->input('status_topic_adviser');
        $Project->status_topic_admin = $request->input('status_topic_admin');
        $Project->projecttype_id = $request->input('projecttype_id');

        $Project->save();
        $id = $Project->id;

        $student = $request->input('students_id');
        $size = count(collect($student));

        for ($i = 0; $i < $size; $i++) {
            $student1 = new membersproject();
            $student1->projects_id = $id;
            $student1->students_id =  $student[$i];
            $student1->save();
        }

        $officer_id = $request->input('officer_id');
        $size = count(collect($officer_id));

        for ($i = 0; $i < $size; $i++) {
            $Adviser = new Adviser();
            $Adviser->projects_id = $id;
            $Adviser->officer_id = $officer_id[$i];
            $Adviser->save();
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
