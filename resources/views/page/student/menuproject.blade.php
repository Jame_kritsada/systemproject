@extends('Layout.masterstd')
@section('title')
@endsection('title')

@section('content')

<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
</div>

<div class="w3-row-padding w3-padding-64 w3-theme-l1" id="work">

    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3"> สร้างโปรเจค</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">สร้างโปรเจค <br> ตามแบบที่เราต้องการ</p>
            </div>
            <div class="col-4">
                <a href="{{ route('createProject.index')}}">
                    <i class="bx bx-plus" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3">โปรเจคของฉัน</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">รวบรวมโปรเจคต่างๆ <br> ที่ฉันเคยสร้าง</p>
            </div>
            <div class="col-4">
                <a href="{{ route('myProject.index')}}" type="text">
                    <i class="bx bx-list-ul" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>


    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3"> โปรเจคทั้งหมด</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">ดูโปรเจคทั้งหมด <br> ของผู้อื่นที่มีในระบบ</p>
            </div>
            <div class="col-4">
                <a href="{{ route('Projectall.index')}}" type="text">
                    <i class="bx bx-layer" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>




    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3">ยื่นสอบ</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">ทำการยื่นสอบต่างๆ <br> ที่มีการจัดสอบ</p>
            </div>
            <div class="col-4">
                <a href="{{ url('exam/show')}}" type="text">
                    <i class="bx bx-book-open" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>

    <div style="margin-top: 100px;" class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3">สอบ</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">ดูตารางการสอบ ตรวจสอบคะแนนและส่งไฟล์หลังสอบ</p>
             
                <a href="{{ url('exam/showcalendar')}}" type="text">
                    <i class="bx bx-bot" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>
    <div style="margin-top: 100px;" class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3">ส่งไฟล์สมบูรณ์</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">ส่งไฟล์โปรเจคฉบับสมบูรณ์</p> <br>
            
                <a href="{{ url('filefull')}}" type="text">
                    <i class="bx bx-book-add" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>

</div>


@endsection