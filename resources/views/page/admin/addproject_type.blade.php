@extends('Layout.master')
@section('title')
เพิ่มประเภทโปรเจ็ค
@endsection('title')

@section('content')
<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
</div>



<form action="{{ route('project_type.store') }}" method="post">
    {{ csrf_field() }}
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="inputPassword4">ประเภทโปรเจค</label>
            <input type="text" class="form-control" name="projecttype_name" required="" placeholder="ตัวอย่าง A">
        </div>
        <div class="form-group col-md-4">
            <label for="inputPassword4">รายละเอียดประเภทโปรเจค</label>
            <input type="text" class="form-control" name="projecttype_detail" required="" placeholder="ตัวอย่าง IOT">
        </div>

        <div class="table-responsive col-6">
            <label for="inputPassword4">อาจารย์ประจำประเภทโปรเจค</label>

            <table class="table  tabel-bordered  table-striped">
                <tr>
                    <th>ชื่อ</th>
                    <th>เลือก</th>
                </tr>
                @foreach ($officers as $row)
                <tr>
                    <td>{{ $row->officer_name }} {{ $row->officer_lastname }}</td>
                    <td><input class="form-check-input" name="officer_id[]" type="checkbox" value="{{ $row->id }}" id="flexCheckDefault"></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="form-group col-md-4"><br>
        <button type="submit" class="btn btn-primary text-center " onclick="return confirm('ต้องการเพิ่มข้อมูลใช่ หรือ ไม่')">เพิ่มข้อมูล</button>
    </div>

</form>

<div class="table-responsive mt-5">
    <table class="table  tabel-bordered  table-striped">
        <tr></tr>
        <th>ชื่อประเภทโปรเจ็ค</th>
        <th>แก้ไข</th>
        <th>ลบ</th>
        </tr>
        @foreach ($project_type as $row)
        <tr>

            <td>{{ $row->projecttype_name }}</td>

            <td>
                <a class="btn btn-warning" data-target="#modal-edit-customers{{$row->id}}" data-toggle="modal" id="modal-edit" title="Edit"><i class="fa fa-wrench"></i></a>

            </td>
            <td>
                <form method="post" class="delete_form" action="{{ action('ProjecttypesController@destroy', $row->id) }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <button class="btn btn-danger" type="submit" onclick="return confirm('ต้องการลบข้อมูลใช่ หรือ ไม่')"><i class="fa fa-trash"></i></button>
                </form>
            </td>
        </tr>


        <div class="modal fade col-12" id="modal-edit-customers{{$row->id}}" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg  w3-animate-right">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="">รายละเอียด</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <input type="hidden" name="_token">
                    <form action="{{ action('ProjecttypesController@update', $row->id) }}" method="post">

                        @csrf
                        @method('PATCH')
                        <!-- Modal body -->
                        <div class="modal-body">

                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">ชื่อประเภทโปรเจ็ค</label>
                                    <input type="text" class="form-control" name="projecttype_name" value="{{ $row->projecttype_name }}">
                                </div>

                            </div>





                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="w3-button w3-green w3-round-xlarge">แก้ไข</button>
                            <button type="button" class="w3-button w3-red w3-round-xlarge" data-dismiss="modal">ออก</button>
                        </div>
                </div>
            </div>
            </form>
            @endforeach
    </table>
</div>

@endsection