@extends('Layout.master')
@section('title')
โปรเจคในที่ปรึกษา
@endsection('title')

@section('content')


<!-- ======= Portfolio Section ======= -->


<div class="section-title">
    <h2>รายชื่อโปรเจคในที่ปรึกษา</h2>
</div>


<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ชื่อโปรเจค</th>
                <th scope="col">รายละเอียด</th>

            </tr>
        </thead>
        <tbody>
            @foreach($project as $row)
            <tr>

                <td>{{$row->project_nameth}}</td>

                <td class="column6">
                    <button type="button" class="bx bx-comment-detail  btn btn-primary " data-toggle="modal" data-target="#d1">
                    </button>
                </td>



            </tr>


            <div class="modal fade" id="d1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl">

                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4>รายละเอียดการสอบ</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-12 ">
                                    <i class="fas fa-align-center"></i>
                                    ระดับโปรเจค -> {{$row->pro1_pro2_status}}
                                    <hr><i class="fas fa-align-center"></i>
                                    สถานะจบ/ไม่จบ -> {{$row->status_finished_notfinished}}
                                    <hr><i class="fas fa-align-center"></i>
                                    ชื่อโปรเจค -> {{$row->project_nameth}}
                                </div>
                            </div>

                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>

            </div>


        </tbody>
        @endforeach

    </table>



    @endsection