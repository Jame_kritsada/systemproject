@extends('Layout.masterstd')
@section('title')
@endsection('title')
@section('content')
<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
    <h4>&nbsp;&nbsp;รายละเอียดโปรเจคของฉัน</h4>
</div>

<table class="table">
    <thead>
        <tr>
            <th scope="col">ชื่อโปรเจค(ไทย)</th>
            <th scope="col">ชื่อโปรเจค(อังกฤษ)</th>
            <th scope="col">วันที่สร้าง</th>
            <th scope="col">สถานะหัวข้อ</th>
            <th scope="col">รายละเอียด</th>

        </tr>
    </thead>
    <tbody>
        @foreach ($membersprojects as $row)
        <tr>

            <td>{{ $row->project_nameth }}</td>
            <td>{{ $row->project_nameen }}</td>

            <td>{{ $row->created_at }}</td>
            @if ($row->status_topic_admin == 'ไม่ผ่านการอนุมัติ')
            <td>
                <div><a class="btn btn-danger">{{ $row->status_topic_admin }}</a>
                    <a href="{{ action('myProjectController@edit', $row->id) }}" class="btn btn-danger">แก้ไขและส่งอีกครั้ง!</a>
                    <form method="post" class="delete_form" action="{{ action('myProjectController@destroy', $row->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class="btn btn-danger">ลบ!</button>
                    </form>
                </div>
            </td>
            @elseif ($row->status_topic_adviser == 'ไม่ผ่านการอนุมัติ')
            <td> <a class="btn btn-danger">{{ $row->status_topic_adviser }}</a>
                <a href="{{ action('myProjectController@edit', $row->id) }}" class="btn btn-danger">แก้ไขและส่งอีกครั้ง!</a>
                <form method="post" class="delete_form" action="{{ action('myProjectController@destroy', $row->id) }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger">ลบ!</button>
                </form>
            </td>
            @elseif ($row->status_topic_admin == 'รอการอนุมัติ')
            <td><button type="text" class="btn btn-info">{{ $row->status_topic_admin }}</button></td>
            @elseif ($row->status_topic_admin == 'ผ่านการอนุมัติ')
            <td><button type="text" class="btn btn-success">{{ $row->status_topic_admin }}</button></td>
            @endif
            <td class="column6">
                <button type="button" class="bx bx-comment-detail  btn btn-primary " data-toggle="modal" data-target="#d1">
                </button>
            </td>



        </tr>

   
        <div class="modal fade" id="d1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl">

                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4>รายละเอียดการสอบ</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 ">
                                <!-- {{$row->id}} -->
                                <i class="fas fa-align-center"></i>
                                ชื่อโปรเจ็คไทย -> {{$row->project_nameth}} <hr><i class="fas fa-align-center"></i>
                                ชื่อโปรเจ็คอังกฤษ -> {{$row->project_nameen}} <hr><i class="fas fa-align-center"></i>
                                สถานะหัวข้ออาจารย์ที่ปรึกษา -> {{$row->status_topic_adviser}} <hr><i class="fas fa-align-center"></i>
                                สถานะหัวข้อที่อาจารย์ประจำวิชา -> {{$row->status_topic_admin}}
                            </div>
                        </div>

                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>


    </tbody>
    @endforeach

</table>

<style>
    .blink {
        animation: blinker 0.6s linear infinite;
        color: #1c87c9;
        font-size: 30px;
        font-weight: bold;
        font-family: sans-serif;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

    .blink-one {
        animation: blinker-one 1s linear infinite;
    }

    @keyframes blinker-one {
        0% {
            opacity: 0;
        }
    }

    .blink-two {
        animation: blinker-two 1.4s linear infinite;
    }

    @keyframes blinker-two {
        100% {
            opacity: 0;
        }
    }
</style>
@endsection