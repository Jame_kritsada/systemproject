@extends('Layout.masterstd')
@section('title')
หน้าแรก
@endsection('title')

@section('content')
<div class="section-title">
            <h2>INFORMATION TECHNOLOGY</h2>
            <h4>&nbsp;&nbsp;ข่าวสาร</h4>
        </div>

<section id="testimonials" class="testimonials section-bg">
    <div class="container">

      
        <div class="owl-carousel testimonials-carousel">

            <div class="testimonial-item" data-aos="fade-up">
                <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus.
                    Accusantium quam, ultricies eget id, aliquam eget
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                {{-- <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt=""> --}}
                <h3>Saul Goodman</h3>
                <h4>Ceo &amp; Founder</h4>
            </div>

            <div class="testimonial-item" data-aos="fade-up" data-aos-delay="100">
                <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    Export tempor illum tamen malis malis eram qu
                    eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim
                    culpa.
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                {{-- <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt=""> --}}
                <h3>Sara Wilsson</h3>
                <h4>Designer</h4>
            </div>

            <div class="testimonial-item" data-aos="fade-up" data-aos-delay="200">
                <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis
                    minim tempor labore quem eram duis nost.
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                {{-- <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt=""> --}}
                <h3>Jena Karlis</h3>
                <h4>Store Owner</h4>
            </div>

            <div class="testimonial-item" data-aos="fade-up" data-aos-delay="300">
                <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    Fugiat enim eram quae cillum dolore dolor
                    velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum
                    veniam.
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                {{-- <img src="assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt=""> --}}
                <h3>Matt Brandon</h3>
                <h4>Freelancer</h4>
            </div>

            <div class="testimonial-item" data-aos="fade-up" data-aos-delay="400">
                <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    Quis quorum aliqua sint quem legam fore iat legam esse veniam culpa fore
                    nisi cillum quid.
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                {{-- <img src="assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt=""> --}}
                <h3>John Larson</h3>
                <h4>Entrepreneur</h4>
            </div>

        </div>

    </div>
</section><!-- End Testimonials Section -->


@endsection