<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Project;
use App\exam;
use App\officer_exam;
use App\studentadexam;

use App\poinexamadmin;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use function Symfony\Component\String\b;

class ExamadminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['exams'] = DB::table('exams')
            // ->join('exams','officer_exams.exam_id','exams.id')
            ->where('status', 'ใช้งาน')
            ->get();
        //  dd($data['exams']);
        return view('page.admin.examadmin', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function addpoin($id)
    {
        $data['exams'] = DB::table('studentadexams')
            ->leftJoin('projects', 'studentadexams.project_id', 'projects.id')
            ->leftJoin('exams', 'studentadexams.exam_id', 'exams.id')
            ->select()
            ->whereNotIn('studentadexams.id', DB::table('poinexamadmins')->select('studentadexams_id'))
            ->where('studentadexams.exam_id', '=', $id)
            ->get();
        //  dd($data['exams']); 
        $data['examss'] = DB::table('exams')
            ->select()
            ->where('id', '=', $id)
            ->get();
        return view('page.admin.exampoin', $data);
    }


    public function addpoinadmin(Request $request)
    {
        $idu = Auth::user()->id;

        $poinexamadmin = new poinexamadmin();
        $poinexamadmin->poin = $request->input('poin');
        $poinexamadmin->comment = $request->input('comment');
        $poinexamadmin->exam_id = $request->input('exam_id');
        $poinexamadmin->project_nameth = $request->input('project_nameth');
        $poinexamadmin->studentadexams_id = $request->input('studentadexams_id');
        $poinexamadmin->id_user = $idu;
        $poinexamadmin->save();

        $id = $request->input('studentadexams_id');
        $validatedData = $request->validate([]);
        $status_exam_adviser = studentadexam::find($id);
        $status_exam_adviser->status_exam = $request->get('status_exam');
        $status_exam_adviser->save();
        return back();
    }

    public function showpoin($id)
    {

        $idu = Auth::user()->id;

        $data['exams'] = DB::table('poinexamadmins')
            ->leftJoin('exams', 'poinexamadmins.exam_id', 'exams.id')
            ->select()
            ->where('poinexamadmins.exam_id', '=', $id)
            ->where('poinexamadmins.id_user', '=', $idu)
            ->get();
            // dd($data);
        return view('page.admin.showpoin', $data);
    }

    public function report($id)
    {

        $data['exams'] = DB::table('studentadexams')
            ->leftJoin('projects', 'studentadexams.project_id', 'projects.id')
            ->select()
            ->where('exam_id', '=', $id)

            ->get();
        $data['examss'] = DB::table('exams')

            ->select()
            ->where('id', '=', $id)

            ->get();
        //  dd( $data['examss']);
        return view('page.admin.report', $data);
    }



    public function seccess(Request $request, $id)
    {

        $Exam = exam::find($id);
        $Exam->status = ('ปิดใช้งาน');
        $Exam->save();
        return back();
    }
    public function managecreate(Request $request)
    {
        // dd($request->input('officer_id'),$request->input('project_id'));
        // dd($request->input('project_id'));
        $project_id = $request->input('project_id');
        $exam_id = $request->input('exam_id');
        $studentadexams = DB::table('studentadexams')
            // ->join('exams','officer_exams.exam_id','exams.id')
            ->select('id')
            ->where('project_id', $project_id)
            ->where('exam_id', $exam_id)->first();


        $project_nameth = DB::table('projects')
            // ->join('exams','officer_exams.exam_id','exams.id')
            ->select('id', 'project_nameth')
            ->where('id', $project_id)->first();
        // dd($project_nameth);
        $officer_id = $request->input('officer_id');

        $size = count(collect($officer_id));
        //   dd($project_nameth->project_nameth);
        for ($i = 0; $i < $size; $i++) {
            $officer1 = new officer_exam;
            $officer1->detail = $request->input('detail');
            $officer1->date = $request->input('date');
            $officer1->project_nameth = $project_nameth->project_nameth;
            $officer1->project_id = $project_id;
            $officer1->studentadexams_id = $studentadexams->id;

            $officer1->exam_id = $request->input('exam_id');
            $officer1->officer_id =  $officer_id[$i];
            $officer1->save();
        }
        return back();
    }
    public function manage($id)
    {
        $data['addexam'] = DB::table('exams')
            // ->join('exams','officer_exams.exam_id','exams.id')
            ->select()
            ->where('id', $id)->get();
        $data['officers'] = DB::table('officers')
            // ->join('exams','officer_exams.exam_id','exams.id')
            ->select()
            ->get();

        $data['studentadexams'] = DB::table('studentadexams')
            ->leftjoin('projects', 'studentadexams.project_id', 'projects.id')
            ->select()
            ->whereNotIn('studentadexams.project_id', DB::table('officer_exams')->select('project_id'))
            ->where('status_exam_admin', 'ผ่านการอนุมัติ')
            ->where('exam_id', $id)
            ->get();
        // dd( $data['studentadexams'] );
        $data['officer_exams'] = DB::table('studentadexams')
            ->leftjoin('projects', 'studentadexams.project_id', 'projects.id')
            ->select()
            ->where('exam_id', $id)
            // ->orderBy('date','Desc')
            ->get();
        $data['officer_examsxx'] = DB::table('officer_exams')
            ->select(
                'detail',
                'date'
            )
            ->where('exam_id', $id)
            ->groupBy('detail','date')
            ->get();
            $data['officerinprojectxx'] = DB::table('officer_exams')
            ->select(
                'detail',
                'date'
            )
            ->where('exam_id', $id)
            ->groupBy('detail','date')
            ->get();
        //dd($data['officer_examsxx']);
        return view('page.admin.manageexam', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $exam_id)
    {
        $poin = poinexamadmin::find($exam_id);
        $poin->poin = $request->input('poin');
        $poin->save();
        return back()->with('success', 'บันทึกสำเร็จ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $project_type = officer_exam::find($id);
        //dd($project_type);
        $project_type->delete();
        return back()->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
