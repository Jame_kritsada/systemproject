<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class membersproject extends Model
{
    protected $fillable = [
        'projects_id',
        'students_id',
        ];
      protected $primaryKey = 'id';
}
