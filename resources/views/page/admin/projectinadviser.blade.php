@extends('Layout.master')
@section('title')
เพิ่มประเภทโปรเจ็ค
@endsection('title')

@section('content')
<div class="container">
    <form>
        <div class="form-row">
            <div class="col-md-3 mb-3">
                <label for="validationDefault03">ประเภทโปรเจค</label>
                <select name="c_status" class="custom-select mb-3" required="">

                    <option selected>------------ เลือก ----------</option>
                    @foreach($advisers as $protype)
                    <option value="{{$protype->id}}">{{$protype->projecttype_name}}</option>

                    @endforeach
                </select>

            </div>
            <div class="col-md-3 mb-3">
                <label for="validationDefault03">อาจารย์</label>
                <select name="c_status" class="custom-select mb-3" required="">
                    <option>------------ เลือก ----------</option>
                    @foreach($officers as $offi)
                    <option value="{{$offi->id}}">{{$offi->officer_name}}  {{$offi->officer_lastname}}</option>

                    @endforeach
                </select>
            </div>

        </div>

        <div class="form-group">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required>
                <label class="form-check-label" for="invalidCheck2">
                    Agree to terms and conditions
                </label>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Submit form</button>
    </form>
    @endsection