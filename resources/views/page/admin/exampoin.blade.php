@extends('Layout.master')
@section('title')
สอบ
@endsection('title')

@section('content')

<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
    @foreach ($examss as $a)
    <a href="{{url('examsadmin/poin/show', $a->id)}}" class="btn btn-info">โปรเจคที่ให้คะแนนแล้ว</a>
    @endforeach
</div>
@foreach ($exams as $row)
<div class="card mt-3">
    <div class="card-body shadow">

        <div class="form-row col-12">
                <p style=" color: rgb(10,10,10);">ชื่อโปรเจค {{$row ->project_nameth}}</p>  
        </div>

        <div class="form-row  col-5">
            <p style=" color: rgb(10,10,10);">ไฟล์แนบ</p>

         <a href="{{asset('fileexam/'.$row->filefirst)}}" download style=" color: rgb(60,60,60);  margin-left:2%;">ดาวน์โหลดไฟล์</a>
                      
        </div>
        <div class="form-row col-5">
            <p style=" color: rgb(10,10,10);">คะแนนอาจารย์ผู้คุมสอบ</p>
        </div>
        <div class="form-row col-5">
            <p>
                <?php
                $con = mysqli_connect("127.0.0.1", "root", "", "systemproject");
                // Check connection
                if (mysqli_connect_errno()) {
                    echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }
                $ids = $row->id;
                $exam_id = $row->exam_id;
                $result = mysqli_query($con, "SELECT * FROM poinexamofficers WHERE $exam_id = exam_id and $ids = id");;

                while ($rows = mysqli_fetch_array($result)) {;
                    echo "<p>" . $rows['poin'] . "</p>&nbsp;&nbsp;" . "";
                    echo "<p>" . $rows['officer_name'] . "</p>";
                }


                mysqli_close($con);
                ?>
            </p>

        </div>
        <form action="{{ url('examadmin/addpoin') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="hidden" name="studentadexams_id" value="{{$row ->id}}">
                <input type="hidden" name="project_id" value="{{$row ->project_id}}">
                <input type="hidden" name="project_nameth" value="{{$row ->project_nameth}}">
                <input type="hidden" name="exam_id" value="{{$row ->exam_id}}">

                <input type="text" name="poin" class="form-control col-2 " placeholder="กรอกคะแนน">
            </div>
            <textarea type="text" name="comment" class="form-control col-10 "  placeholder="ความคิดเห็น"></textarea>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="status_exam" id="inlineRadio1" value="ผ่าน">
                <label class="form-check-label" for="inlineRadio1">ผ่าน</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="status_exam" id="inlineRadio2" value="ไม่ผ่าน">
                <label class="form-check-label" for="inlineRadio2">ไม่ผ่าน</label>
            </div>


            <button type="submit" class="btn btn-info col-1 mt-2">บันทึก</button>

        </form>

        <div class="form-group row col-1">


        </div>


    </div>
</div>

@endforeach

<hr>
@endsection