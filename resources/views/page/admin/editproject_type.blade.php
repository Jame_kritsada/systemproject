@extends('Layout.master')
@section('title')
แก้ไขประเภทโปรเจ็ค
@endsection('title')

@section('content')


    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">แก้ไขประเภทโปรเจ็ค</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ action('ProjecttypesController@update', $project_type->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <label for="inputPassword4">ชื่อประเภทโปรเจ็ค</label>
                                    <input type="text" class="form-control" name="projecttype_name"
                                        value="{{ $project_type->projecttype_name }}" >
                                </div>
                              
                            </div>
                            <div class=" text-center">
                                <button type="submit" class="btn btn-primary "
                                    onclick="return confirm('ต้องการแก้ไขข้อมูลใช่ หรือ ไม่')">เพิ่มข้อมูล</button>
                            </div>
                            <input type="hidden" name="_method" value="PATCH">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
