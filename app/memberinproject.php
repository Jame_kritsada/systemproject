<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class memberinproject extends Model
{
    protected $fillable = [
        'project_id',
        'std_id',
        ];
      protected $primaryKey = 'id';
}
