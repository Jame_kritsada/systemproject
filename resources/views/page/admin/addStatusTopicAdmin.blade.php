@extends('Layout.master')
@section('title')
คำขอเป็นที่ปรึกษา
@endsection('title')

@section('content')


<div class="section-title">
    <h2>คำขอเป็นที่ปรึกษา</h2>
</div>
<table class="table table-hover">
    <thead>
        <tr>

            <th class="column3">ชื่อโปรเจ็ค</th>
            <th class="column3">รายละเอียดโปรเจ็ค</th>
            <th class="column5">อนุมัติ</th>
            <th class="column6">ไม่อนุมัติ</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($project as $row)
        @if($row->status_topic_admin == 'รอการอนุมัติ')
        <tr>
            <td>{{ $row->project_nameth }}</td>
            <td>
                <a href="{{ url('detel_admin', $row->project_nameth) }}" type="submit" style='font-size:15px' class="w3-button w3-blue w3-round-xlarge far " method="get">แสดงรายละเอียด</a>
            </td>
            {{-- <td>{{ $row->gpa1 }}</td> --}}
            <td class="column5">
                <form action="{{ url('upstatus_admin', $row->project_nameth) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <button onclick="myFunction()" type="submit" style='font-size:15px' class="w3-button w3-green w3-round-xlarge far " name="status_topic_admin" value="ผ่านการอนุมัติ">อนุมัติ</button>
                    <script>
                        function myFunction() {
                            alert("You pressed OK!");
                        }
                    </script>
                </form>
            </td>

            <td class="column6">
                <!-- <form action="{{ url('upstatus_admin', $row->project_nameth) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <button onclick="myFunction2()" type="submit" style='font-size:15px' class="w3-button w3-red w3-round-xlarge far " name="status_topic_admin" value="ไม่ผ่านการอนุมัติ">ไม่อนุมัติ</button>
                        <script>
                            function myFunction2() {
                                alert("You pressed Cancel!");
                            }
                        </script>
                    </form> -->
                <button type="button" class="w3-button w3-red w3-round-xlarge far " data-toggle="modal" data-target="#1">
                    ไม่อนุมัติ
                </button>
            </td>

        </tr>
        @endif




        <div class="modal fade" id="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl">

                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="" required="">ความคิดเห็น</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form action="{{  url('upstatus_admin', $row->project_nameth) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-12 ">
                                    <input type="hidden" name="status_topic_admin" value="ไม่ผ่านการอนุมัติ">
                                    <textarea name="detail" class="form-control" id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>



                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </tbody>@endforeach
</table>

<br>
<div class="section-title">
    <h2>สถานะหัวข้อโปรเจค</h2>
</div>

<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ชื่อโปรเจค</th>
                <th scope="col">สถานะอาจารย์ที่ปรึกษา</th>
                <th scope="col">สถานะอาจารย์ประจำวิชา</th>
                <th scope="col">รายละเอียด</th>

            </tr>
        </thead>
        <tbody>
            @foreach($projects as $row)
            <tr>

                <td>{{$row->project_nameth}}</td>
                <td>{{$row->status_topic_adviser}}</td>
                <td>{{$row->status_topic_admin}}</td>
                <td class="column6">
                    <button type="button" class="bx bx-comment-detail  btn btn-primary " data-toggle="modal" data-target="#d2">
                    </button>
                </td>



            </tr>


            <div class="modal fade" id="d2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl">

                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4>รายละเอียดการสอบ</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-12 ">
                                   <i class="fas fa-align-center"></i>
                                    ระดับโปรเจค -> {{$row->pro1_pro2_status}}<hr><i class="fas fa-align-center"></i>
                                    ชื่อโปรเจค -> {{$row->project_nameth}} <br><hr><i class="fas fa-align-center"></i>
                                    ชื่อโปรเจค -> {{$row->project_nameen}} <hr><i class="fas fa-align-center"></i>
                                    รายละเอียดโปรเจค -> {{$row->project_detailth}} <hr><i class="fas fa-align-center"></i>
                                    รายละเอียดโปรเจค -> {{$row->project_detailen}} <hr><i class="fas fa-align-center"></i>
                                    สถานะสอบกลางภาค -> {{$row->status_midterm}} <hr><i class="fas fa-align-center"></i>
                                    สถานะสอบปลายภาค -> {{$row->status_final}} 
                                </div>
                            </div>

                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>

            </div>


        </tbody>
        @endforeach

    </table>
</div>
@endsection