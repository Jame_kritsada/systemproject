<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Student;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Symfony\Component\VarDumper\Cloner\Data;

class Member1Controller extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data['addmember'] = DB::table('students')
      ->join('users', 'students.user_id', 'users.id')
      ->select(
        'students.id',
        'users.email',
        'users.password',
        'students.student_id',
        'students.student_name',
        'students.student_lastname',
        'students.tel_std',
      )
      ->get();
    // dd($data);
    return view('page.admin.member', $data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
    if ($request->file('image') != '') {

      $file = $request->file('image');
      $ext = $file->getClientOriginalExtension();

      $name = md5(rand() * time()) . '.' . $ext;
      $file->move(public_path('image/cars'), $name);
    } else {
      $name = '';
    }
    $post = new User([
      'c_name' => $request->c_name,
      'c_register' => $request->c_register,
      'TyCar_id' => $request->TyCar_id,
      'c_brand' => $request->c_brand,
      'c_status' => $request->c_status,
      'c_images' => $name
    ]);

    $post->save();
    //return redirect ("/post")->with('บันทึกสำเร็จ');
    return redirect()->route('showcar')->with('feedback', 'เพิ่มข้อมูลสำเร็จ');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $data['editmember'] = DB::table('students')
      ->join('users', 'students.user_id', 'users.id')
      ->select(
        'students.id',
        'users.email',
        'users.password',
        'students.student_id',
        'students.student_name',
        'students.student_lastname',
        'students.tel_std',
      )
      ->where('students.id', $id)
      ->get();
    return view('page.admin.editmember', $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    
    
    $posts = user::find($id);

    $posts->email = $request->input('email');
    $posts->password = Hash::make($request->input('password'));
    $posts->save();

    return redirect()->route('member.index')->with('feedback', 'บันทึกข้อมูลสำเร็จ');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
    $post = student::find($id);
    $post->delete();
    $post = User::find($id);
    //dd($post);
    $post->delete();
    

    return redirect()->route('member.index')->with('feedback', 'ลบข้อมูลสำเร็จ');
  }
}
