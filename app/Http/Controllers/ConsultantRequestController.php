<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\comentadviser;
use App\project;
use Illuminate\Support\Facades\Hash;

class ConsultantRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $id =  Auth::user()->id;
        $ids = DB::table('officers')->select('id')
            ->where('user_id', '=', $id)->first();

        // $idc = DB::table('officer_groupprojecttypes')->select('id')
        //     ->where('officer_id', '=', $ids->id)->first();
        // dd($ids->id);


        $data['project'] = DB::table('advisers')
            ->join('projects', 'advisers.projects_id', 'projects.id')
            ->select(
                'projects.pro1_pro2_status',
                'projects.status_finished_notfinished',
                'projects.project_nameth'
            )->where('advisers.officer_id', $ids->id)
            ->where('projects.status_topic_adviser', '=', 'รอการอนุมัติ')
            ->get();

            $data['projects'] = DB::table('advisers')
            ->join('projects', 'advisers.projects_id', 'projects.id')
            ->select(
            )->where('advisers.officer_id', $ids->id)
            ->where('projects.status_topic_adviser' ,'!=', 'รอการอนุมัติ')
            ->get();
        // dd($data['advisers']);
        return view('page.profesor.ConsultantRequest', $data)->with('success', 'บันทึกสำเร็จ');
    }

    public function detel(Request $request, $project_nameth)
    {

        $ids = DB::table('projects')->select('id')
            ->where('project_nameth', '=', $project_nameth)->first();

        $data['projectss'] = DB::table('projects')
            ->where('project_nameth', '=', $project_nameth)
            ->get();

        $data['student'] = DB::table('membersprojects')
            ->join('students', 'membersprojects.students_id', 'students.id')

            ->where('membersprojects.projects_id', '=', $ids->id)
            // ->groupBy('membersprojects.students_id')
            ->get();
        //  dd( $data['student']);
        return view('page.profesor.detelConsultantRequest', $data);
    }

    public function upstatus_adviser(Request $request, $project_nameth)
    {
        //   dd($project_name);
        $id =  Auth::user()->id;

        $ids = DB::table('projects')->select('id')
            ->where('project_nameth', '=', $project_nameth)->first();
        //   dd($ids);
        if ($request->get('status_topic_adviser') == 'ไม่ผ่านการอนุมัติ') {

            $validatedData = $request->validate([]);
            $projects = Project::find($ids->id);
            $projects->status_topic_adviser = $request->get('status_topic_adviser');
            $projects->save();


            $comentadvisers = new comentadviser();
            $comentadvisers->detail = $request->input('detail');
            $comentadvisers->id_project = $ids->id;
            $comentadvisers->id_user = $id;

            $comentadvisers->save();
        } elseif (($request->get('status_topic_adviser') == 'ผ่านการอนุมัติ')) {
            $validatedData = $request->validate([]);
            $projects = Project::find($ids->id);
            $projects->status_topic_adviser = $request->get('status_topic_adviser');
            $projects->save();
        }
        return back()->with('success', 'บันทึกสำเร็จ');
    }
}
