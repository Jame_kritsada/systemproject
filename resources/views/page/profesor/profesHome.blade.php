@extends('Layout.master')
@section('title')
หน้าแรก
@endsection('title')

@section('content')
<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
</div>
<div class="w3-row-padding w3-padding-64 w3-theme-l1" id="work">

    @if(Auth::user()->type == 'officer')
    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3"> คำร้องขอเป็นที่ปรึกษา</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">อนุมัติคำร้อง <br> ที่ขอเป็นโปรเจคในที่ปรึกษา</p>

                <a href="{{ url('/ConsultantRequest') }}">
                    <i class="bx bx-face" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>

            </div>
        </div>
    </div>

    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3">คำร้องขอขึ้นสอบ</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">อนุมัติคำร้อง <br> โปรเจคที่ขอขึ้นสอบ</p>

                <a href="{{ url('/StudentExamOfficer') }}">
                    <i class="bx bx-book-add" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>

            </div>
        </div>
    </div>
    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3">โปรเจคทั้งหมด</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">ดูโปรเจคทั้งหมด
                    ที่มีในระบบ</p>

                <a href="{{ url('/allproject') }}" type="text">
                    <i class="bx bx-layer" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>

            </div>
        </div>
    </div>
    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3">สอบ</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">แสดงหรือให้คะแนนสอบ <br> ที่เปิดทำการสอบ</p>

                <a href="{{route('examofficers.index')}}" type="text">
                    <i class="bx bx-bot" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>

            </div>
        </div>
    </div>

    @elseif(Auth::user()->type == 'admin')
    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3"> อนุมัติหัวข้อโปรเจค</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">อนุมัติคำร้อง <br> ที่ขอสร้างหัวข้อโปรเจค</p>

                <a href="{{ route('ConsultantRequestAdmin.index') }}">
                    <i class="bx bx-face" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3">คำร้องขอขึ้นสอบ</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">อนุมัติคำร้อง <br> โปรเจคที่ขอขึ้นสอบ</p>

                <a href="{{ url('/StudentExamAdmin') }}">
                    <i class="bx bx-book-add" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>


    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3"> เพิ่มประเภทโปรเจค</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">เพิ่มประเภทของโปรเจคและอาจารย์ประจำประเภทโปรเจค</p>

                <a href="{{ route('project_type.index') }}" type="text">
                    <i class="bx bx-list-ol" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>



    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3">จัดการสอบ</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">สร้าง ลบ แก้ไขการสอบต่างๆ เพื่อให้นิสิตทำการยื่นขอสอบ
                </p>
            
                <a href="{{ route('Exam.index') }}" type="text">
                    <i class="bx bx-layer" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>


    <div style="margin-top: 100px;" class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">

                <dt class="card-title mt-3">สอบ</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">แสดงหรือให้คะแนนสอบ <br> ที่เปิดทำการสอบ</p>
            </div>
            <div class="col-4">
                <a href="{{url('exam/showexam')}}" type="text">
                    <i class="bx bx-bot" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>
    <div style="margin-top: 100px;" class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3">โปรเจคทั้งหมด</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">ดูโปรเจคทั้งหมด
                    ที่มีในระบบ</p>

                <a href="{{ url('/allprojectAdmin') }}" type="text">
                    <i class="bx bx-layer" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>

            </div>
        </div>
    </div>
</div>

@endif
@endsection