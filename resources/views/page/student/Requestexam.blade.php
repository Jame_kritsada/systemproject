@extends('Layout.masterstd')
@section('title')
@endsection('title')
@section('content')
<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
    <h3 style="font-family: 'Sarabun', sans-serif;">ยื่นขอสอบ</h3>
    <h3 style="font-family: 'Sarabun', sans-serif;">รายละเอียดการสอบ</h3>
    @foreach ($exams as $exam)
    <h3 style="font-family: 'Sarabun', sans-serif;">&nbsp;&nbsp;<i class=" bx bx-right-arrow-alt"></i> @if($exam -> topic == 'midterm')สอบกลางภาค @elseif($exam -> topic == 'final')สอบปลายภาค @endif ปีการศึกษา {{$exam -> yearedu}}</h3>
    <h3 style="font-family: 'Sarabun', sans-serif;">&nbsp;&nbsp;<i class=" bx bx-right-arrow-alt"></i> @if($exam -> typeproject == 'Project1')โปรเจค1 @elseif($exam -> typeproject == 'Project2')โปรเจค2 @endif </h3>
    <h3 style="font-family: 'Sarabun', sans-serif;">&nbsp;&nbsp;<i class=" bx bx-calendar"></i>&nbsp;เริ่มสอบวันที่ {{ $exam -> dateexamstart}} ถึง {{$exam -> dateexamend}}</h3>
    <h3 style="font-family: 'Sarabun', sans-serif;">&nbsp;&nbsp;<i class=" bx bx-calendar"></i>&nbsp;เริ่มยื่นขอสอบวันที่ {{ $exam -> dateregisstart}} ถึง {{$exam -> dateregisend}}</h3>
    <h4 style="font-family: 'Sarabun', sans-serif;">&nbsp;&nbsp;&nbsp; {{ $exam -> detail}}</h4>

    <form action="{{ url('Requestexams/addexam') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}

        @foreach ($projects as $project)
        <input type="hidden" class="form-control" name="project_id" value="{{ $project -> id}}">
        @endforeach
        <input type="hidden" class="form-control" name="exam_id" value="{{ $exam -> id}}">
        <input type="hidden" class="form-control" name="status_exam_adviser" value="รอการอนุมัติ">
        <input type="hidden" class="form-control" name="status_exam_admin" value="รอการอนุมัติ">
        <hr>
        <div class="form-group col-8">
            <h4 for="exampleFormControlSelect1" style="font-family: 'Sarabun', sans-serif;">เลือกโปรเจค</h4>
            <select class="form-control" id="exampleFormControlSelect1" name="project_id" required="">
               
                @foreach ($projects as $pro)
                <option value="{{$pro->id}}">{{$pro->project_nameth}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-7">

            <input type="file" name="file_name" class="custom-file-input" id="inputGroupFile01">
            <label class="custom-file-label" for="inputGroupFile01">แนบไฟล์</label>

        </div>

        <div class=" text-center">
            <button type="submit" class="btn btn-primary " onclick="return confirm('ต้องการเพิ่มข้อมูลใช่ หรือ ไม่')"><i class="bx bx-plus-circle">ยื่นขอสอบ</i></button>
        </div>
    </form>
    @endforeach
</div>


@endsection