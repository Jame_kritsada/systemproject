<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class officer_groupprojecttype extends Model
{
    protected $fillable = [
        'projecttype_id',
        'officer_id',
        ];
      protected $primaryKey = 'id';
}
