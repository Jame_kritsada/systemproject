@extends('Layout.masterstd')
@section('title')
@endsection('title')
@section('content')
<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
    <h4>&nbsp;&nbsp;โปรเจคทั้งหมด</h4>
</div>
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ลำดับ</th>
                <th scope="col">ชื่อโปรเจค</th>
                <th scope="col">รายละเอียด</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($membersprojects as $row)
            <tr>

                <td></td>
                <td>{{ $row->project_nameth }}</td>
                <td class="column6">
                    <button type="button" class="bx bx-comment-detail  btn btn-primary " data-toggle="modal" data-target="#d1">
                    </button>
                </td>
                <!-- <td>
                                        <center><a href="" class="btn btn-warning">แก้ไข</a></center>
                                    </td>
                                    <td>
                                        <center>
                                            <form action="" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">ลบ</button>

                                            </form>
                                        </center>
                                    </td> -->
            </tr>

            <div class="modal fade" id="d1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl">

                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4>รายละเอียดการสอบ</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-12 ">
                                    <i class="fas fa-align-center"></i>
                                    ชื่อโปรเจ็คไทย -> {{$row->project_nameth}}
                                    <hr><i class="fas fa-align-center"></i>
                                    ชื่อโปรเจ็คอังกฤษ -> {{$row->project_nameen}}
                                    <hr><i class="fas fa-align-center"></i>
                                    สถานะหัวข้ออาจารย์ที่ปรึกษา -> {{$row->status_topic_adviser}}
                                    <hr><i class="fas fa-align-center"></i>
                                    สถานะหัวข้อที่อาจารย์ประจำวิชา -> {{$row->status_topic_admin}}
                                </div>
                            </div>

                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>

            </div>




        </tbody>
        @endforeach
    </table>
</div>
<style type="text/css">
    table tr {
        counter-increment: row-num;
    }

    table tr td:first-child::before {
        content: counter(row-num) ". ";
    }
</style>


@endsection