<?php

namespace App\Http\Controllers;

use App\comentadmin;
use Illuminate\Http\Request;
use App\comentadviser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\project;
use Illuminate\Support\Facades\Hash;

class ConsultantRequestAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {




        // $idc = DB::table('officer_groupprojecttypes')->select('id')
        //     ->where('officer_id', '=', $ids->id)->first();
        // dd($ids->id);


        $data['project'] = DB::table('projects')
            ->where('status_topic_adviser', '=', 'ผ่านการอนุมัติ')
            ->get();

            $data['projects'] = DB::table('projects')
            ->where('status_topic_adviser', '=', 'ผ่านการอนุมัติ')
            ->where('status_topic_admin', '!=', 'รอการอนุมัติ')
            ->get();
        // dd($data['advisers']);
        return view('page.admin.addStatusTopicAdmin', $data)->with('success', 'บันทึกสำเร็จ');
    }

    public function detel(Request $request, $project_name)
    {

        $ids = DB::table('projects')->select('id')
            ->where('project_nameth', '=', $project_name)->first();

        $data['projectss'] = DB::table('projects')
            ->where('project_nameth', '=', $project_name)
            ->get();

        $data['student'] = DB::table('membersprojects')
            ->join('students', 'membersprojects.students_id', 'students.id')

            ->where('membersprojects.projects_id', '=', $ids->id)
            // ->groupBy('membersprojects.students_id')
            ->get();
        //  dd( $data['student']);
        return view('page.admin.detelConsultantRequestAdmin', $data);
    }

    public function upstatus_admin(Request $request, $project_name)
    {
        //   dd($project_name);
        $id =  Auth::user()->id;
        $ids = DB::table('projects')->select('id')
            ->where('project_nameth', '=', $project_name)->first();
        //   dd($ids);
        if ($request->get('status_topic_admin') == 'ไม่ผ่านการอนุมัติ') {
            $validatedData = $request->validate([]);
            $projects = Project::find($ids->id);
            $projects->status_topic_admin = $request->get('status_topic_admin');
            $projects->save();

            $comentadvisers = new comentadmin();
            $comentadvisers->detail = $request->input('detail');
            $comentadvisers->id_project = $ids->id;
            $comentadvisers->id_user = $id;

            $comentadvisers->save();
        } elseif (($request->get('status_topic_admin') == 'ผ่านการอนุมัติ')) {
            $validatedData = $request->validate([]);
            $projects = Project::find($ids->id);
            $projects->status_topic_admin = $request->get('status_topic_admin');
            $projects->save();
        }
        return back()->with('success', 'บันทึกสำเร็จ');
    }
}
