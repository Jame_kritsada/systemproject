@extends('Layout.master')
@section('title')
จัดการสมาชิก
@endsection('title')

@section('content')


<div class="container">
    <h2>ข้อมูลสมาชิก</h2>
    <div class="col-md-8 ">
        <a href="{{ url('createmember') }}" class="btn btn-success "><i class="fa fa-plus-square"></i>
            เพิ่มข้อมูล
        </a>
    </div>
    <br>
    <p></p>
</div>
<table class="table table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>email</th>
            <th>รายละเอียด</th>
            <th>แก้ไข</th>
            <th>ลบ</th>
        </tr>
    </thead>
    <tbody>

        <tr>
            @foreach($addmember as $row)
            <td>{{ $row->student_id }}</td>
            <td>{{ $row->email }}</td>


            <td>

                <button type="button" style='font-size:24px' class="w3-button w3-blue w3-round-xlarge far fa-address-card" data-toggle="modal" data-target="#modal-edit-customers{{$row->id}}">
                </button>
            </td>
            <td>
                <a href="{{ action('Member1Controller@edit', $row->id) }}" class="btn btn-warning"><i class="fa fa-wrench"></i></a>
            </td>

            <td>

              
            </td>
        </tr>






        <!-- The Modal ***************************************************************************************************************************-->

        <div class="modal fade" id="modal-edit-customers{{$row->id}}" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl">

                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="">รายละเอียด</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <input type="hidden" name="_token">
                    <form action="#" method="POST" id="formEditPost">

                        @csrf
                        @method('PATCH')
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-4 mb-4">
                                    <label for="validationDefault02">ชื่อ-นามสกุล</label>
                                    <input type="text" class="form-control" name="B_NameMember" id="B_NameMember" value="{{$row->student_name}} {{$row->student_lastname}}">
                                </div>
                                <div class="col-md-2 mb-4">
                                    <label for="validationDefault02">รหัสนิสิต</label>
                                    <input type="text" class="form-control" name="B_StatusMember" id="B_StatusMember" value="{{$row->student_id}}">
                                </div>
                                <div class="col-md-2 mb-4">
                                    <label for="validationDefault03">เบอร์โทร</label>
                                    <input type="tel" class="form-control" name="B_TelMember" id="B_TelMember" maxlength="10" value="{{$row->tel_std}}">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-3 mb-4">
                                    <label for="validationDefault03">E-mail</label>
                                    <input type="text" class="form-control" name="LC_CarUse" id="LC_CarUse" value="{{$row->email}}">
                                </div>
                                <div class="col-md-2 mb-4">
                                    <label for="validationDefault04">Password</label>
                                    <input type="text" class="form-control" name="LC_Passenger" id="LC_Passenger" value="{{$row->password}}">
                                </div>

                            </div>

                    </form>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>

        <!-- End Modal *************************************************************************************************************************** -->
        </div>
        @endforeach
    </tbody>
</table>
</div>


@endsection

@section('footerscript')
@if (session('feedback'))

<script src="{{ asset('alert.js') }}"></script>

<script>
    swal("{{ session('feedback') }}", "ผลการทำงาน", "success");
</script>

@endif

@endsection