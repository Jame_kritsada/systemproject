<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function PHPSTORM_META\type;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $type = Auth::user()->type;
        if (($type == 'student')) {
            return view('page.student.menuproject');
        }elseif  (($type == 'officer')) {
            return view('page.profesor.profesHome');
        }elseif (($type == 'admin')){
            return view('page.profesor.profesHome');
        }
    }
}
