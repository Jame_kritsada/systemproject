CREATE view listbursaryview as SELECT regis_bursaries.*,considers.user_id as userid_con FROM regis_bursaries
LEFT JOIN considers ON regis_bursaries.id = considers.rb_id
WHERE not regis_bursaries.id 
IN (SELECT rb_id FROM considers)

create view considershowview as SELECT considers.*,regis_bursaries.bur_id  FROM considers
LEFT JOIN regis_bursaries ON considers.rb_id = regis_bursaries.id
WHERE  considers.rb_id = regis_bursaries.id
