<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
      'pro1_pro2_status',
      'project_nameth',
      'project_nameen',
      'project_detailth',
      'project_detailen',
      'file_project',
      'file_full',
      'status_topic_adviser',
      'status_topic_admin',
      'status_midterm',
      'status_final',
    ];
      protected $primaryKey = 'id';
}
 