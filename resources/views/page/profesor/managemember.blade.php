@extends('Layout.master')
@section('title')
หน้าแรก
@endsection('title')

@section('content')

<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
</div>

<div class="w3-row-padding w3-padding-64 w3-theme-l1" id="work">

    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3"> จัดการอาจารย์</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">เพิ่ม ลบ แก้ไข อาจารย์</p>
            </div>
            <div class="col-4">
                <a href="{{ route('officer.index') }}">
                    <i class="bx bx-user-plus" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>



    <div class="w3-quarter">
        <div class="w3-card w3-white">
            <div class="w3-container">
                <dt class="card-title mt-3">จัดการนิสิต</dt>
                <p class="card-text" style=" color: rgb(100,100,100);">เพิ่ม ลบ แก้ไข นิสิต</p>
            </div>
            <div class="col-4">
                <a href="{{ route('member.index') }}">
                    <i class="bx bx-bot" style="font-size: 70px; color: rgb(120,132,232);"></i>
                </a>
            </div>
        </div>
    </div>


</div>


@endsection