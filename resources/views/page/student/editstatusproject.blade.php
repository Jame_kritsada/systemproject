@extends('Layout.masterstd')
@section('title')
@endsection('title')
@section('content')
<div class="section-title">
    <h2>INFORMATION TECHNOLOGY</h2>
    <h4>&nbsp;&nbsp;สร้างโปรเจค</h4>
</div>
<div class="container">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    @foreach($Editmembersprojects as $row)
    <form action="{{action('myProjectController@update',$row->id)}}" method="post">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <div class="form">
            <input type="hidden" class="form-control" name="status_topic_adviser" value="รอการอนุมัติ">
            <input type="hidden" class="form-control" name="status_topic_admin" value="รอการอนุมัติ">
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="inputPassword4">โปรเจค : {{$row->pro1_pro2_status}}</label>
                    
                </div>
                
                <div class="form-group col-md-4">
                @foreach ($myproject_types as $p)
                    <label for="inputPassword4">ประเภทโปรเจค : {{$p->projecttype_name}}</label>
                        @endforeach
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                @foreach ($officer as $of)
                    <label for="inputPassword4">อาจารย์ที่ปรึกษา : {{$of->officer_name}} {{$of->officer_lastname}}</label>
                        @endforeach
                </div>
                
                <div class="form-group col-md-4">
                @foreach ($mystudents as $p)
                    <label for="inputPassword4">ชื่อสมาชิก : {{$p->student_name}} {{$p->student_lastname}}</label>
                        @endforeach
                </div>
            </div>

            
            <div class="row">
                <div class="form-group  col-md-5">
                    <label for="inputPassword4">ชื่อโปรเจค(ไทย)</label>
                    <input type="text" class="form-control" name="project_nameth" value="{{ $row->project_nameth }}" required="">
                </div>
                <div class="form-group  col-md-5">
                    <label for="inputPassword4">ชื่อโปรเจค(อังกฤษ)</label>
                    <input type="text" class="form-control" name="project_nameen" value="{{ $row->project_nameen }}" required="">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-8">
                    <label for="inputPassword10">รายละเอียดโปรเจค(ไทย)</label>
                    <textarea class="form-control" rows="7" id="comment" name="project_detailth" required="">{{ $row->project_detailth }}</textarea>
                </div>
                <div class="form-group col-md-8">
                    <label for="inputPassword10">รายละเอียดโปรเจค(อังกฤษ)</label>
                    <textarea class="form-control" rows="7" id="comment" name="project_detailen" required="">{{ $row->project_detailen }}</textarea>
                </div>
            </div>
        </div>

        <div class=" text-center">
            <button type="submit" class="btn btn-primary " onclick="return confirm('ต้องการเพิ่มข้อมูลใช่ หรือ ไม่')"><i class="bx bx-plus-circle">เพิ่มข้อมูล</i></button>
        </div>
    </form>
    @endforeach
</div>


<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css') }}" />
<script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js') }}"></script>
<script src="{{ asset('https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js') }}">
</script>


<script>
    $('select').selectpicker();
    $('#Xselect').selectpicker();
</script>





@endsection